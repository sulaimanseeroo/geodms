﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services;
using Microsoft.SharePoint;

namespace CustomLoginPage.Layouts.WebServices
{
    class CustomWebservice
    {
        public class MyCustomWebService : WebService
        {
            [WebMethod]
            public string GetSiteListCount()
            {
                var web = SPContext.Current.Web;

                return (web.Title + " contains " +
                    web.Lists.Count.ToString() + " lists.");
            }
        }
    }
}
