﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailToAdmin.aspx.cs" Inherits="CustomLoginPage.Layouts.CustomLoginPage.EmailToAdmin"  %>

<html>
<head>
<title>Email To Admin | PSS - DMS</title>
<style>
#lblpwd2Msg
{
    padding-left:8px;
    }
</style>
    <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="faveicon.png" type="image/x-icon" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/view_port.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700'
        rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link href="/_layouts/CustomLoginPage/css/bvalidator/bvalidator.css" rel="stylesheet"
        type="text/css" />
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery-latest.min.js" type="text/javascript"></script>
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery.bvalidator.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        $(document).ready(function () {
            $('#Form1').bValidator();
        });
    </script>
</head>
<body class="inner_wrappermain">
    <form id="Form1" runat="server">  
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="email_login">
                    <%--<div class="logosub">
                        <img src="images/logo.png" width="100%" height="auto" /></div>--%>
                        <div class="email_body">
                    <div class="email_temporary">
                    <div>
                        <h3>
                            Email To Admin</h3>
                            <p>
                                <asp:Label ID="lblMsg" runat="server" Font-Bold="True" ForeColor="#006600"></asp:Label>
                             </p>
                            
                        <%--<p class="whatis">    
                        <asp:Label ID="lblFrom" CssClass="whatwas" Text="From" runat="server"></asp:Label>
                        <asp:TextBox class="user_box" runat="server" Width="400px"  ID="txtFrom"></asp:TextBox>
                        
                        <asp:RequiredFieldValidator ID="rfv_NewPassword" ControlToValidate="txtFrom"
                                runat="server" ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator></p>--%>
                        <p class="whatis"><asp:Label ID="lblSubject" CssClass="whatwas"  Text="Subject" runat="server"></asp:Label>
                        <asp:TextBox class="user_box" runat="server" Width="400px" data-bvalidator="required" ID="txtSubject"></asp:TextBox></p>                       
                        <p class="whatis"><asp:Label ID="lblMessage" CssClass="whatwas"  Text="Message" runat="server"></asp:Label>
                        <asp:TextBox class="user_box" runat="server" Width="400px"  ID="txtMesssage" data-bvalidator="required" TextMode="MultiLine" Height="90px" ></asp:TextBox>                         
                        </p>  
                        
                        <div class="clearfix">
                        </div>  
                                           
                        <asp:Button ID="btnSend" CssClass="continue_btn" OnClick="btnSend_Click" runat="server" Text="Send" />
                    </div>
                     </div>
                     </div>
                </div>
                <div id="part_off">
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
