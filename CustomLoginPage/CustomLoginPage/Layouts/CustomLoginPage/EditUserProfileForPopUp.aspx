﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditUserProfileForPopUp.aspx.cs"
    Inherits="CustomLoginPage.Layouts.CustomLoginPage.EditUserProfileForPopUp" %>

<html>
<head>
    <title>My Profile</title>
    <link rel="icon" href="/_layouts/CustomLoginPage/faveicon.png" type="image/x-icon" />
    <link href="/_layouts/CustomLoginPage/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/view_port.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700'
        rel='stylesheet' type='text/css' />
    <style>
        .email_login
        {
            top: 280px !important;
        }
        .update_btn
        {
            margin: 10px 10px 30px 0!important;
            float: left !important;
        }
        .continue_btn1
        {
            background: none repeat scroll 0 0 #1F6FF7;
            border-bottom: 3px solid #0051CB;
            border-radius: 3px;
            color: #FFFFFF;
            display: block;
            float: left;
            font-size: 18px;
            font-weight: 500;
            line-height: 32px;
            margin-bottom: 30px;
            margin-left: 0px;
            margin-top: 10px;
            border-top: none;
            border-left: none;
            border-right: none;
            margin-right: 15px;
            text-align: center;
            height: 38px;
            padding-left: 5px;
            padding-right: 5px;
        }
        button, html input[type="button"], input[type="reset"], input[type="submit"]
        {
            background: none repeat scroll 0 0 #1F6FF7;
            border-bottom: 3px solid #0051CB;
            border-radius: 3px;
            color: #FFFFFF;
            display: block;
            float: right;
            font-size: 18px;
            font-weight: 500;
            line-height: 32px;
             border-top: none;
            border-left: none;
            border-right: none;
            margin: 10px 15px 30px 11px !important;
            text-align: center;
            height: 38px;
        }
    </style>
    <script type="text/javascript">
        function CloseModel() {
            $.modal.close();
            return false;
        }

    </script>
    <link href="/_layouts/CustomLoginPage/css/bvalidator/bvalidator.css" rel="stylesheet"
        type="text/css" />
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery-latest.min.js" type="text/javascript"></script>
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery.bvalidator.js" type="text/javascript"></script>
</head>
<script type="text/javascript">
    var options = {
        offset: { x: 5, y: -2 },
        position: { x: 'left', y: 'top' }
    };
    $(document).ready(function () {
        $('#form1').bValidator(options);
    });
</script>
<body class="inner_wrappermain">
    <form id="form1" runat="server">
    <div id="wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="email_login">
                        <div class="email_temporary">
                            <h3>
                                My Profile</h3>
                            <h5>
                            </h5>
                            <p>
                                <asp:Label ID="lblMsg" runat="server" Font-Bold="True" ForeColor="#006600"></asp:Label></p>
                            <p>
                                <span class="user_inn">First Name</span>
                                <asp:TextBox ID="txtFirstName" CssClass="user_innbox" data-bvalidator="required"
                                    runat="server"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Last Name</span>
                                <asp:TextBox ID="txtLastName" CssClass="user_innbox" runat="server"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Email<%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                    runat="server" ControlToValidate="txtEmail" ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator>--%></span>
                                <asp:TextBox ID="txtEmail" data-bvalidator="email,required" CssClass="user_innbox"
                                    runat="server"></asp:TextBox>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="a"
                                    Display="Dynamic" ControlToValidate="txtEmail" ErrorMessage="Invalid Email ID"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>--%>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Mobile Phone</span>
                                <asp:TextBox ID="txtMobilePhone" data-bvalidator="required" CssClass="user_innbox"
                                    runat="server"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Work Phone</span>
                                <asp:TextBox ID="txtWorkPhone" runat="server" CssClass="user_innbox"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <%--<div id="SecurityDiv" runat="server">
                                <div class="ques_ans">
                                    <div class="form_question">
                                        <span>Security Question1</span>
                                    </div>
                                    <div class="form_answer">
                                        <asp:Label ID="lblSecurityQuestion1" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="ques_ans">
                                    <div class="form_answer_">
                                        <span>Answer<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                            ControlToValidate="txtSecurityQuestion1" ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator></span>
                                    </div>
                                    <div class="form_answer">
                                        <asp:TextBox ID="txtSecurityQuestion1" CssClass="user_innbox" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="ques_ans">
                                    <div class="form_question">
                                        <span>Security Question2</span>
                                    </div>
                                    <div class="form_answer">
                                        <asp:Label ID="lblSecurityQuestion2" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="ques_ans">
                                    <div class="form_answer_">
                                        <span>Answer
                                            <asp:RequiredFieldValidator ID="rfv_txtSecurityQuestion2" runat="server" ControlToValidate="txtSecurityQuestion2"
                                                ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator></span></div>
                                    <div class="form_answer">
                                        <asp:TextBox ID="txtSecurityQuestion2" CssClass="user_innbox" runat="server"></asp:TextBox>
                                    </div>
                                </div>--%>
                            <div class="browse_pic">
                                <span class="user_inn">Profile Picture</span>
                                <asp:FileUpload ID="fupProfilePicture" runat="server" class="browsebtn" />
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></div>
                            <%--<p>
                                <span class="user_inn">Answer<asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                    runat="server" ControlToValidate="txtSecurityAnswer" ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator></span>
                                <asp:TextBox ID="txtSecurityAnswer" CssClass="user_innbox" runat="server"></asp:TextBox></p>--%>
                            <div class="clearfix">
                            </div>
                            <div style="margin-left: 38.2%;">
                                <%--<div class="back_btn">
                                <a href="forgot_password.html">Back</a></div>--%>
                                <asp:Button ID="btnEditUser" runat="server" ValidationGroup="a" Text="Update User"
                                    CssClass="update_btn" OnClick="btnEditUser_Click" />
                                <asp:HyperLink ID="HyperLink2" CssClass="continue_btn1" runat="server" NavigateUrl="~/_layouts/CustomLoginPage/ResetPasswordForPopUp.aspx">Reset Password</asp:HyperLink>
                                <%-- <div class="back_btn"><a href="http://sp01:24011/_layouts/CustomLoginPage/IndexPage.aspx">Back</a></div>    --%>
                                <%--<a href="#" class="continue_btn">SUBMIT</a>--%></div>
                            <div id="part_off">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
