﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="CustomLoginPage.Layouts.CustomLoginPage.LoginPage" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8"/>
    <title>PSS - DMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="icon" href="faveicon.png" type="image/x-icon" />
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <link href="css/view_port.css" rel="stylesheet" type="text/css"/>
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700'
        rel='stylesheet' type='text/css'/>
        <script src="http://code.jquery.com/jquery-1.7.1.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.20/jquery-ui.min.js"
        type="text/javascript"></script>
   <%-- <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>--%>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script>
        window.onload = function () {
            var hdl = $('#<%=hdfld.ClientID%>').html();
            if (hdl == 'true')
                $('#hypEmailToAdmin').click();
           
        }
    </script>
    <script src="/_layouts/CustomLoginPage/js/jquery.simplemodal.1.4.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OpenModal(src, Isadmin) {

            if (Isadmin == 'false') {
                $.modal('<iframe src="' + src + '" height="auto" width="100%" style="border:0">', {
                    closeHTML: " <img src='images/close.jpg' alt='' class='btnclose' />",
                    containerCss: {
                        backgroundColor: "#fff",
                        borderColor: "#fff",
                        height: 560,
                        padding: 2,
                        WebkitBorderTopLeftRadius: 15,
                        WebkitBorderTopRightRadius: 15,
                        WebkitBorderBottomLeftRadius: 15,
                        WebkitBorderBottomRightRadius: 15,
                        MozBorderRadius: 15,
                        BorderRadius: 15,
                        width: '40%',
                        closeByDocument: false,
                        overlayClose: false
                    },

                    overlayCss: { backgroundColor: "#666" }
                });
                return false;
            }


            function CloseModel() {
                $.modal.close();
            }
        }
    </script>
     <script type="text/javascript">

         function popup(url) {
             var NWin = window.open(url, '', 'scrollbars=1,height=600,width=800');
             if (window.focus) {
                 NWin.focus();
             }
             return false;

         }
</script>
 <%--fancy box --%>
    <script type="text/javascript" src="/_layouts/CustomLoginPage/js/fancybox/jquery-1.10.1.min.js"></script>

	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="/_layouts/CustomLoginPage/js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="/_layouts/CustomLoginPage/js/fancybox/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="css/fancyboxcss/jquery.fancybox.css?v=2.1.5" media="screen" />
   
	<!-- Add Button helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="css/fancyboxcss/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="/_layouts/CustomLoginPage/js/fancybox/jquery.fancybox-buttons.js?v=1.0.5"></script>

	<!-- Add Thumbnail helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="css/fancyboxcss/jquery.fancybox-thumbs.css?v=1.0.7" />
	<script type="text/javascript" src="/_layouts/CustomLoginPage/js/fancybox/jquery.fancybox-thumbs.js?v=1.0.7"></script>

	<!-- Add Media helper (this is optional) -->
	<script type="text/javascript" src="/_layouts/CustomLoginPage/js/fancybox/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            /*
            *  Simple image gallery. Uses default settings
            */

            $('.fancybox').fancybox();

            /*
            *  Different effects
            */

            // Change title type, overlay closing speed
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    overlay: {
                        speedOut: 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                helpers: {
                    title: {
                        type: 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS: 'fancybox-custom',
                closeClick: true,

                openEffect: 'none',

                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay: {
                        css: {
                            'background': 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });

            // Remove padding, set opening and closing animations, close if clicked and disable overlay
            $(".fancybox-effects-d").fancybox({
                padding: 0,

                openEffect: 'elastic',
                openSpeed: 150,

                closeEffect: 'elastic',
                closeSpeed: 150,

                closeClick: true,

                helpers: {
                    overlay: null
                }
            });

            /*
            *  Button helper. Disable animations, hide close button, change title type and content
            */

            $('.fancybox-buttons').fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                prevEffect: 'none',
                nextEffect: 'none',

                closeBtn: false,

                helpers: {
                    title: {
                        type: 'inside'
                    },
                    buttons: {}
                },

                afterLoad: function () {
                    this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
                }
            });


            /*
            *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
            */

            $('.fancybox-thumbs').fancybox({
                prevEffect: 'none',
                nextEffect: 'none',

                closeBtn: false,
                arrows: false,
                nextClick: true,

                helpers: {
                    thumbs: {
                        width: 50,
                        height: 50
                    }
                }
            });

            /*
            *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
            */
            $('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
				    openEffect: 'none',
				    closeEffect: 'none',
				    prevEffect: 'none',
				    nextEffect: 'none',

				    arrows: false,
				    helpers: {
				        media: {},
				        buttons: {}
				    }
				});

            /*
            *  Open manually
            */

            $("#fancybox-manual-a").click(function () {
                $.fancybox.open('1_b.jpg');
            });

            $("#fancybox-manual-b").click(function () {
                $.fancybox.open({
                    href: 'iframe.html',
                    type: 'iframe',
                    padding: 5
                });
            });

            $("#fancybox-manual-c").click(function () {
                $.fancybox.open([
					{
					    href: '1_b.jpg',
					    title: 'My title'
					}, {
					    href: '2_b.jpg',
					    title: '2nd title'
					}, {
					    href: '3_b.jpg'
					}
				], {
				    helpers: {
				        thumbs: {
				            width: 75,
				            height: 50
				        }
				    }
				});
            });


        });
	</script>
    <style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}

		body {
			/*max-width: 700px;*/
			margin: 0 auto;
		}
	</style>
</head>
<body>
<form id="Form1" runat="server">
    <section id="section_log01">
<div class="logo"><img src="images/logo.png" width="64" height="96" alt=""/></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12"><div class="banner_log">
      <div id="circle">
  <div class="circle one"> <img src="images/sh_01.png" width="78" height="169"/></div>
    <div class="circle two"> <img src="images/sh_02.png" width="78" height="169"/></div>
  <div class="circle three"> <img src="images/sh_03.png" width="78" height="169"/></div>
  <div class="circle four"> <img src="images/sh_04.png" width="78" height="169"/></div>
</div>
     <div class="banner_pic"> <img src="images/pc_row.png" width="100%" height="auto" alt=""/></div></div>
          <div class="banner_mob"> <img src="images/pc_row_copy.png" width="100%" height="auto" alt=""/></div></div>

      </div>
    </div>
 
  <div class="clearfix"></div>
</section>
    <section id="section_log02">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-lg-offset-3">
        <div class="login_box">
        <div class="region"><img src="images/ranger.png"/></div>
                <div class="map_part"><img src="images/map_parj.png"/></div>

          <h1>login<span></span></h1>
          <div class="field_section_lft">
            <p>
            <asp:TextBox ID="txtUsername" CssClass="login_username" placeholder="Username" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rquser" runat="server" ValidationGroup="a" Display="Dynamic"
                    ControlToValidate="txtUsername" ErrorMessage="Enter your username"></asp:RequiredFieldValidator>
              <%--<input class="login_username" type="text" value="" name="_username" placeholder="User Name">--%>
            </p>
          </div>
          <div class="field_section_rit">
            <p>
            <asp:TextBox ID="txtPassWord" CssClass="login_password" placeholder="Password" TextMode="Password" runat="server"></asp:TextBox>
              <%--<input class="login_password" type="password" value="********" name="password">--%>
              <asp:RequiredFieldValidator ID="rqpassword" ValidationGroup="a" runat="server" Display="Dynamic"
                    ControlToValidate="txtPassWord" ErrorMessage="Enter your password"></asp:RequiredFieldValidator>
              <%--<input class="login_password" type="password" value="" name="password" placeholder="********">--%>
            </p>
          </div>
          <div class="signin_sec">
            <p>
            <asp:Button ID="btnLogin" ValidationGroup="a" runat="server" CssClass="sign_inbtn"  OnClick="btnLogin_Click" Text="Sign In"></asp:Button>
              <%--<a href="index.html" class="sign_inbtn">LOGIN</a>--%> <asp:Label ID="lblMsg" Text="" ForeColor="Red" runat="server" ></asp:Label>

            </p>
            <p class="chec_part">
             <asp:CheckBox ID="chkRemember" CssClass="remember_me" runat="server"></asp:CheckBox>
              <%--<input class="remember_me" type="checkbox" value="Remember Me" name="remember me">--%>
              Remember Me</p>
          </div>
          <div class="login_bottom">
            <p><span class="for_lft"><%--<a href="ForgotUsername.aspx">Forgot Username ?</a>--%> 
            <%--<asp:LinkButton ID="lnkForgotUsrname" OnClick="lnkForgotUsrname_Click" CssClass="forgotpass" runat="server">Forgot Username ?</asp:LinkButton>--%>
            <asp:Button ID="btnForgotUsrname" OnClick="btnForgotUsrname_Click" CssClass="forgotpass"  runat="server" Text="Forgot Username ?"></asp:Button>
            </span>
             <span class="for_rit"><%--<a href="forgotpassword.aspx" Class="forgotpass">Forgot Password ?</a>--%>
              <%--<asp:LinkButton ID="lnkForgotPwd" OnClick="lnkForgotPwd_Click" CssClass="forgotpass" runat="server">Forgot Password ?</asp:LinkButton>--%>
              <asp:Button ID="btnForgotPwd" runat="server" OnClick="btnForgotPwd_Click" CssClass="forgotpass" Text="Forgot Password ?"></asp:Button>
            <%--<a href="forgot_password.html"> Forgot Password ? </a>--%></span> </p>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</section>
    <footer id="footer_sec">
      <div class="container">
    <div class="row">
          <div class="col-lg-12">
        <div class="copy_right">
              <p>© 2013  PSS - DMS. All Rights Reserved. </p>
            </div>
        <div class="social_mediasec">
              <div class="sm_icn"><a href="#"><img src="images/facebook.jpg" alt=""/></a></div>
              <div class="sm_icn"><a href="#"><img src="images/twitter.jpg" alt=""/></a></div>
              <div class="sm_icn"><a href="#"><img src="images/linkedin.jpg" alt=""/></a></div>
              <div class="sm_icn"><a href="#"><img src="images/skype.jpg" alt=""/></a></div>
            </div>
            <%--<asp:HiddenField ID="hdfld" EnableViewState="false" Value="false" runat="server"></asp:HiddenField>--%>

            <div id="hdfld"  runat="server" style="display:none;">
            false
            </div>

        <ul class="footer_menu">
              <li  class="first_child"><a href="#" onclick="popup('/_layouts/pss dms helppages/index.html');" id="helpIndex" >Help</a></li>
          <li><%--<asp:LinkButton ID="lnkEmailToAdmin" OnClick="lnkEmailToAdmin_Click"  runat="server">Email Admin</asp:LinkButton>--%>
          <asp:Button ID="btnEmailToAdmin" CssClass="btnEmailtoAdmin" OnClick="btnEmailToAdmin_Click" runat="server" Text="Email Admin"></asp:Button>
          <a id="hypEmailToAdmin" class="fancybox fancybox.iframe" href="/_layouts/CustomLoginPage/EmailToAdmin.aspx"  ></a>
          </li>
            </ul>
      </div>
        </div>
  </div>
    </footer>
    <!--------------- toogle menu script --------------->
    <script type="text/javascript" src="js/script.js"></script>
    </form>
</body>
</html>
