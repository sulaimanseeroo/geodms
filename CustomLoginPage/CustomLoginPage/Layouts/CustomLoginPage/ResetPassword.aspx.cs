﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Data;
using System.Web;
using System.Configuration;

namespace CustomLoginPage.Layouts.CustomLoginPage
{
    public partial class ResetPassword : LayoutsPageBase
    {
        ConnectionClass obj = new ConnectionClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            txtNewPassword.Attributes.Add("OnKeyUp", "PasswordChanged(this)");
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime createdDate = DateTime.Now;
                string userName = Session["username"].ToString();
                obj.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
                obj.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
                obj.cmd.Parameters.Add("@NewPassword", SqlDbType.VarChar).Value = txtConfirmPassword.Text;
                obj.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
                if (obj.exequery("sp_ResetPassword"))
                {
                    //ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Your password is successfully reset');", true);
                    lblMsg.Text = "Your password is successfully reset";
                    Response.Redirect("LoginPage.aspx");

                }
            }
            catch 
            {

                HttpContext.Current.ClearError();
            }
        }
    }
}
