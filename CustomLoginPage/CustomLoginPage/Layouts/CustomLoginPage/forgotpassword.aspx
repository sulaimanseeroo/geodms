﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="forgotpassword.aspx.cs" Inherits="CustomLoginPage.Layouts.CustomLoginPage.forgotpassword"%>
<!DOCTYPE HTML>
<html>

<meta charset="utf-8">
<title>Forgot Password | PSS - DMS</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="faveicon.png" type="image/x-icon" />
<link href="/_layouts/CustomLoginPage/css/style.css" rel="stylesheet" type="text/css">
<link href="/_layouts/CustomLoginPage/css/view_port.css" rel="stylesheet" type="text/css">

<link href="/_layouts/CustomLoginPage/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="/_layouts/CustomLoginPage/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
<link href="/_layouts/CustomLoginPage/css/tooltipster-punk.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700' rel='stylesheet' type='text/css'>
<link href="/_layouts/CustomLoginPage/css/bvalidator/bvalidator.css" rel="stylesheet" type="text/css" />
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery-latest.min.js" type="text/javascript"></script>
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery.bvalidator.js" type="text/javascript"></script>
 <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    
<%--<style>
#lblUserID
{
    margin-bottom:5px;
    display:block;
    }
</style>--%>
<%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>--%>
<head>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form1').bValidator();
        $(".part_03").hide();
        $(".part_02").hide();
        $(".part_01").slideToggle('fast');      
        $("#<%= rdbSecurityQuestion.ClientID %>").click(function () {
            $(".part_03").hide();
            $(".part_02").hide();
            $(".part_01").slideToggle('fast');
        });
    });

    $(document).ready(function () {
       
        $("#<%= rdbEmailTemporaryPassword.ClientID %>").click(function () {
            $(".part_03").hide();
            $(".part_01").hide();

            $(".part_02").slideToggle('fast');
        });
    });

    $(document).ready(function () {
        $("#<%= rdbSMSTemporaryPassword.ClientID %>").click(function () {
            $(".part_01").hide();
            $(".part_02").hide();
            $(".part_03").slideToggle('fast');
        });

        $("#a_01").click(function () {
            $(".part_01").hide('slow');

        });
        $("#a_02").click(function () {
            $(".part_02").hide('slow');

        });
        $("#a_03").click(function () {
            $(".part_03").hide('slow');

        });


    });
</script>
</head>
<body class="inner_wrappermain">
<form id="form1" runat="server">
<div class="logo"><img src="images/logo.png" width="64" height="96" alt=""/></div>
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="login_body">
<div class="login_frm">
<%--<div class="logosub"><img src="images/logo.png" width="100%" height="auto"/></div>--%>
 <div class="user_id">
 <p><%--<input class="user_box" type="text" value="User ID" name="user id">--%>
 <asp:Label ID="lblUserID" CssClass="whatwas" runat="server" Text="Enter Your Username"></asp:Label>
 <asp:TextBox runat="server" class="user_box"  name="userid" data-bvalidator="required" ToolTip="Enter Your Username" id="userid"></asp:TextBox>
     <%--<asp:RequiredFieldValidator ID="rfv_UserID" Display="Dynamic" runat="server" ErrorMessage="Enter username and continue" ControlToValidate="userid"></asp:RequiredFieldValidator>--%>
 </p>
 <p>
 <asp:Label ID="lblMsg" runat="server" ForeColor="#FF3300"></asp:Label>
 </p>
 <div class="sec_tips">
 
  
  
<p> <%--<input class="radio_part" type="radio" value="Security Question" name="Security Question">--%>
 <asp:RadioButton runat="server" class="radio_part" Text="Security Question" GroupName="rdforgotpassword" ID="rdbSecurityQuestion" />
<%-- <a href="s TextAlign="Left" ecurity_question.html" target="_blank">Security Question</a>--%></p>
  <p>
  <%--<input class="radio_part" type="radio" value="Email Temporary Password" name="Email Temporary Password">--%>
  <asp:RadioButton runat="server" class="radio_part" Text="Email Temporary Password" GroupName="rdforgotpassword" ID="rdbEmailTemporaryPassword" />
  <%--<a href="email_temporary_password.html" target="_blank">Email Temporary Password</a>--%></p>
 <p>
 <%--<input class="radio_part" type="radio" value="SMS Temporary Password" name="SMS Temporary Password">--%>
 <asp:RadioButton runat="server" class="radio_part" Text="SMS Temporary Password" GroupName="rdforgotpassword" ID="rdbSMSTemporaryPassword" />
 <%--<a href="sms_temporary_password.html" target="_blank">SMS Temporary Password</a>--%></p>
    
 </div>
 <div class="part_01">These questions help us to know your Identity
          <a href="#" id="a_01"><img src="images/close.jpg" width="16" height="16" alt=""/></a></div>
          <div class="part_02"> Password reset requested.<br/>
            Please enter the email address used to login to the PSS -DMS portal for Referring
             providers. A new password will be send to that e-mail address.<a href="#" id="a_02"><img src="images/close.jpg" width="16" height="16" alt=""/></a> </div>
          <div class="part_03">PSS - DMS will send a new password  to your phone when you forgot your password.
          <a href="#" id="a_03"><img src="images/close.jpg" width="16" height="16" alt=""/></a> </div>
 <div class="clearfix"></div>
 <div class="back_btn"><a href="/_layouts/CustomLoginPage/LoginPage.aspx">Back</a></div>
<%--<a href="#"  class="continue_btn">CONTINUE</a> --%>
<asp:Button runat="server" ID="btncontinue" class="continue_btn" Text="Continue" OnClick="btncontinue_Click" />
</div>  
</div>
</div>
<div id="part_off"></div>
</div>
</div>
</div>
</form>
</body>
</html>


