﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Data;
using System.Net.Mail;
using System.Configuration;
using System.Web;

namespace CustomLoginPage.Layouts.CustomLoginPage
{
    public partial class ForgotUsername : LayoutsPageBase
    {
        ConnectionClass obj = new ConnectionClass();
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        public static bool SendMail(string toMailAddress, string subject, string body)
        {
            bool isSent = false;

            try
            {
                //bool isServer = false;
                //bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["IsServer"], out isServer);


               // string mailFrom = "test@seeroo.com";
                //System.Configuration.ConfigurationManager.AppSettings.Get("MailFrom");
                string mailFrom = ConfigurationSettings.AppSettings["mailFrom"];
                //string mailDisplayName = "PSS-DMS";
                //System.Configuration.ConfigurationManager.AppSettings.Get("MailDisplayName");
                string mailDisplayName = ConfigurationSettings.AppSettings["mailDisplayName"];
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

                //checking if the mail form is not null and not an empty string
                if (!string.IsNullOrEmpty(mailFrom))
                {
                    //setting the dispaly name after null checking
                    msg.From = String.IsNullOrEmpty(mailDisplayName) ? new MailAddress(mailFrom) : new MailAddress(mailFrom, mailDisplayName);
                }

                //checking if the subject is not null and not an empty string
                if (!string.IsNullOrEmpty(subject))
                {
                    msg.Subject = subject;
                }

                msg.To.Add(toMailAddress);
                msg.Body = "Your username is : "+body;
                msg.IsBodyHtml = true;

                System.Net.Mail.SmtpClient mailSender = new System.Net.Mail.SmtpClient();

               // string mailHost = "mail.seeroo.com";
                //System.Configuration.ConfigurationManager.AppSettings.Get("MailHost");
                string mailHost = ConfigurationSettings.AppSettings["mailHost"];
                //string mailPassword = "heLYpMLB";
                //System.Configuration.ConfigurationManager.AppSettings.Get("MailPassword");
                string mailPassword = ConfigurationSettings.AppSettings["mailPassword"];
                //setting credential if password and the mail from is not null
                if (!String.IsNullOrEmpty(mailFrom) && !String.IsNullOrEmpty(mailPassword))
                {
                    mailSender.Credentials = new System.Net.NetworkCredential(mailFrom, mailPassword);
                }
                //setting the host if the config host value is not null
                if (!String.IsNullOrEmpty(mailHost))
                {
                    mailSender.Host = mailHost;
                }

                mailSender.Send(msg);
                isSent = true;
            }
            catch (Exception ex)
            {
                isSent = false;
            }
            return isSent;
        }
        public void SendUserName()
        {
            try
            {
                bool isSent;
                string username = "";
                obj.cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = txtEmail.Text;
                DataTable dtCheckMail = obj.ViewData("aspnet_Users_VerifyEmail");
                if (dtCheckMail.Rows.Count > 0)
                {
                    username = dtCheckMail.Rows[0]["UserName"].ToString();
                    if (username != "")
                    {
                        isSent = SendMail(txtEmail.Text, "Username", username);
                        if (isSent == true)
                        {
                            lblMsg.Text = "Your username has been sent to your mail address";
                        }
                    }
                    else
                        lblMsg.Text = "Failed to send the username,please try again";
                }
                else
                    lblMsg.Text = "Invalid MailID";
            }
            catch 
            {

                HttpContext.Current.ClearError();
            }     
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                SendUserName();
                ClearData();
            }
            catch
            {
                 lblMsg.Text = "Failed to send the username,please try again";
               
            }
        }
        public void ClearData()
        {
            txtEmail.Text = "";
        }
    }
}
