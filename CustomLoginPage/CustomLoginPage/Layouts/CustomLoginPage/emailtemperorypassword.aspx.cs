﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Data;
using System.Net.Mail;
using System.Configuration;
using System.Web;
using System.Text;

namespace CustomLoginPage.Layouts.CustomLoginPage
{
    public partial class emailtemperorypassword : System.Web.UI.Page
    {
        public static string userName;
        ConnectionClass obj = new ConnectionClass();
        ConnectionClass obj1=new ConnectionClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            userName = Decryptdata(Request.QueryString["userid"].ToString());
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                bool isSent;
                string newPassword = "";
                obj.cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = userName;
                obj.cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = txtemail.Text;
                DataTable dtCheckMail = obj.ViewData("aspnet_Users_CheckEmailForPasswordSending");
                if (dtCheckMail.Rows.Count > 0)
                {
                    if (Convert.ToInt32(dtCheckMail.Rows[0]["count"].ToString()) > 0)
                    {
                        newPassword = GeneratePassword();
                        if (newPassword != "")
                        {
                            isSent = SendMail(txtemail.Text, "New Password", newPassword);
                            if (isSent == true)
                            {
                                obj1.cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = userName;
                                obj1.cmd.Parameters.Add("@NewPassword", SqlDbType.VarChar).Value = newPassword;
                                if (obj1.exequery("aspnet_Membership_updatePasswordFromRandom"))
                                {
                                    lblMsg.Text = "Your Password has been sent to your mail address";
                                    Response.Redirect("LoginPage.aspx");
                                }
                            }
                        }
                        else
                            lblMsg.Text = "Failed to send the password,please try again";
                    }
                    else
                        lblMsg.Text = "Invalid MailID";
                }
            }
            catch 
            {

                lblMsg.Text = "Failed to send the password,please try again";
            }
        }
        public string GeneratePassword()
        {
            try
            {
                string PasswordLength = "8";
                string allowedChars = "";
                allowedChars = "1,2,3,4,5,6,7,8,9,0";
                allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
                allowedChars += "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
                allowedChars += "~,!,@,#,$,%,^,&,*,+,?";

                char[] sep = { ',' };
                string[] arr = allowedChars.Split(sep);

                string IDString = "";
                string temp = "";

                Random rand = new Random();

                for (int i = 0; i < Convert.ToInt32(PasswordLength); i++)
                {
                    temp = arr[rand.Next(0, arr.Length)];
                    IDString += temp;


                }

                return IDString;
            }
            catch 
            {

                HttpContext.Current.ClearError();
                return null;
            }
        }
        private string Decryptdata(string encryptpwd)
        {
            try
            {
                string decryptpwd = string.Empty;
                UTF8Encoding encodepwd = new UTF8Encoding();
                Decoder Decode = encodepwd.GetDecoder();
                byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
                int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                decryptpwd = new String(decoded_char);
                return decryptpwd;
            }
            catch
            {

                HttpContext.Current.ClearError();
                return null;
            }
        }
        public static bool SendMail(string toMailAddress, string subject, string body)
        {
            bool isSent = false;

            try
            {
                //bool isServer = false;
                //bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["IsServer"], out isServer);


                //string mailFrom = "test@seeroo.com";
                    //System.Configuration.ConfigurationManager.AppSettings.Get("MailFrom");
                string mailFrom = ConfigurationSettings.AppSettings["mailFrom"];
               // string mailDisplayName = "PSS-DMS";
                    //System.Configuration.ConfigurationManager.AppSettings.Get("MailDisplayName");
                string mailDisplayName = ConfigurationSettings.AppSettings["mailDisplayName"];
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

                //checking if the mail form is not null and not an empty string
                if (!string.IsNullOrEmpty(mailFrom))
                {
                    //setting the dispaly name after null checking
                    msg.From = String.IsNullOrEmpty(mailDisplayName) ? new MailAddress(mailFrom) : new MailAddress(mailFrom, mailDisplayName);
                }

                //checking if the subject is not null and not an empty string
                if (!string.IsNullOrEmpty(subject))
                {
                    msg.Subject = subject;
                }

                msg.To.Add(toMailAddress);
                msg.Body = "Your temperory password is : "+body;
                msg.IsBodyHtml = true;

                System.Net.Mail.SmtpClient mailSender = new System.Net.Mail.SmtpClient();

                //string mailHost = "mail.seeroo.com";
                    //System.Configuration.ConfigurationManager.AppSettings.Get("MailHost");
                string mailHost = ConfigurationSettings.AppSettings["mailHost"];
                //string mailPassword = "heLYpMLB";
                    //System.Configuration.ConfigurationManager.AppSettings.Get("MailPassword");
                string mailPassword = ConfigurationSettings.AppSettings["mailPassword"];

                //setting credential if password and the mail from is not null
                if (!String.IsNullOrEmpty(mailFrom) && !String.IsNullOrEmpty(mailPassword))
                {
                    mailSender.Credentials = new System.Net.NetworkCredential(mailFrom, mailPassword);
                }
                //setting the host if the config host value is not null
                if (!String.IsNullOrEmpty(mailHost))
                {
                    mailSender.Host = mailHost;
                }

                mailSender.Send(msg);
                isSent = true;
            }
            catch (Exception ex)
            {
                isSent = false;
            }
            return isSent;
        }
    }
}
