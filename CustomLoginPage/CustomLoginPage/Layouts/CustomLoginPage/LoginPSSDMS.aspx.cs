﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.IdentityModel;
using Microsoft.SharePoint.IdentityModel.Pages;
using System.Text.RegularExpressions;
using System.Web.Security;
using System.Web;
using System.Text;
using System.Data;

namespace CustomLoginPage.Layouts.CustomLoginPage
{
    public partial class LoginPSSDMS : System.Web.UI.Page
    {
        ConnectionClass obj=new ConnectionClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.Cookies["Username"] != null)
                {
                    txtUsername.Text = Decryptdata(Request.Cookies["Username"].Value);
                }
                if (Request.Cookies["Username"] != null)
                    chkRemember.Checked = true;
            }
        }    
    protected void btnLogin_Click(object sender, EventArgs e)
     {            
        obj.cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value=txtUsername.Text;
        obj.cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value=txtPassWord.Text;
        DataTable Login=obj.ViewData("aspnet_userlogin");
        if (Login.Rows[0]["Status"].ToString() == "Success")
        {
            //HttpContext.Current.Session["username"] = txtUsername.Text;
            //if (chkRemember.Checked)
            //{
            //    Response.Cookies["Username"].Value = Encryptdata(txtUsername.Text);
            //    //Response.Cookies["Password"].Value =Encryptdata(txtPassWord.Text);
            //    Response.Cookies["Username"].Expires.AddDays(7);
            //    //Response.Cookies["Password"].Expires.AddDays(7);                   
            //}
            //else
            //{
            //    Response.Cookies["Username"].Value = null;
            //    Response.Cookies["Password"].Value = null;
            //}
            Response.Redirect("IndexPage.aspx",false);
        }
        else
            lblMsg.Text = "Invalid username or password";            
        }

        private string Encryptdata(string password)
        {
            string strmsg = string.Empty;
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            strmsg = Convert.ToBase64String(encode);
            return strmsg;
        }
        private string Decryptdata(string encryptpwd)
        {
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }
        protected void lnkForgotPwd_Click(object sender, EventArgs e)
        {
            Response.Redirect("forgotpassword.aspx", false);
        }
}
}
