﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Data;
using System.Web;
using System.Configuration;

namespace CustomLoginPage.Layouts.CustomLoginPage
{
    public partial class IndexPage : LayoutsPageBase
    {
        public static string userName;
        ConnectionClass obj = new ConnectionClass();
        ConnectionClass obj1=new ConnectionClass();
        ConnectionClass obj2 = new ConnectionClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("~/_layouts/CustomLoginPage/LoginPage.aspx");
                }
                else
                {
                    //lnkAddUser.Visible = false;
                    hypAddUser.Visible = false;
                    lblUsername.Text = Session["username"].ToString();
                    //if (Session["username"].ToString() == "admin1")
                    //{
                    obj1.cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = Session["username"].ToString();
                    DataTable IsAdmin = obj1.ViewData("aspnet_Membership_CheckAdmin");
                    if (IsAdmin.Rows[0]["IsAdmin"].ToString().ToLower()=="true")                
                      
                        {
                            hypAddUser.Visible = true;
                            //lnkEdit.Visible = false;
                            divAddUser.Visible = true;
                            //lnkEdit.Attributes.Add("OnClick", "return OpenModal('EditUserProfileForPopUp.aspx','false')");
                        }
                        else
                        {
                            divAddUser.Visible = false;
                            ////lnkEdit.Enabled = true;
                            //lnkEdit.Attributes.Add("OnClick", "return OpenModal('EditUserProfileForPopUp.aspx','false')");
                        }
                    }

                    if (!IsPostBack)
                    {
                        BindData();
                    }
                }
            
            catch 
            {

                HttpContext.Current.ClearError();
            }
        }

        private void BindData()
        {
            try
            {
                string dateStr = "";
                string correcteddate = "";
                DateTime createdDate = DateTime.Now;
                userName = Session["username"].ToString();
                obj.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
                obj.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
                obj.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
                obj.cmd.Parameters.Add("@UpdateLastActivity", SqlDbType.Bit).Value = 0;
                DataTable securityQstn = obj.ViewData("aspnet_Membership_GetUserByName");
                if (securityQstn.Rows.Count > 0)
                {
                    dateStr = securityQstn.Rows[0]["LastLoginnedDate"].ToString();
                    correcteddate = DateTime.SpecifyKind(DateTime.Parse(dateStr), DateTimeKind.Utc).ToLocalTime().ToString();
                    lbllastlogin.Text = "Last Login : " + dateStr;
                    //lbllastlogin.Text = "Last Logged In : " + securityQstn.Rows[0]["LastLoginDate"].ToString();
                    if (securityQstn.Rows[0]["ProfilePicture"].ToString() == "")
                    {
                        imgProfilePic.ImageUrl = "~/_layouts/CustomLoginPage/ProfilePictures/photo_not_available.jpg";
                        Session["ProfilePic"] = securityQstn.Rows[0]["ProfilePicture"].ToString();
                    }
                    else
                    {
                        imgProfilePic.ImageUrl = securityQstn.Rows[0]["ProfilePicture"].ToString();
                        Session["ProfilePic"] = securityQstn.Rows[0]["ProfilePicture"].ToString();
                    }
                }
            }
            catch 
            {

                HttpContext.Current.ClearError();
            }
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            try
            {
                HttpCookie cookie;
                cookie = Request.Cookies.Get("User");
                if (cookie != null)
                {
                    cookie = new HttpCookie("User");
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(cookie);
                }
                obj2.cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = Session["username"].ToString();
                if(obj2.exequery("aspnet_Membership_Logout"))
                {
                Session.RemoveAll();
                Response.Redirect("~/_layouts/CustomLoginPage/LoginPage.aspx");
                }

            }
            catch 
            {

                HttpContext.Current.ClearError();
            }
        }
        
    }
}
