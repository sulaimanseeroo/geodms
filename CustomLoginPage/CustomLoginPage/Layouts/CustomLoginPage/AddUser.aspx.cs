﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Configuration;
namespace CustomLoginPage.Layouts.CustomLoginPage
{
    public partial class AddUser : System.Web.UI.Page
    {
        ConnectionClass obj = new ConnectionClass();
        ConnectionClass obj1 = new ConnectionClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txtPassWord.Attributes.Add("OnKeyUp", "PasswordChanged(this)");

                if (!IsPostBack)
                {                   
                    ddlRole.DataSource = obj.ViewData("sp_ViewRoles");
                    ddlRole.DataTextField = "RoleName";
                    ddlRole.DataValueField = "RoleId";
                    ddlRole.DataBind();
                    if (ddlRole.Items.Count > 0)
                        ddlRole.Items.Insert(0, new ListItem("Select", "0"));
                    else
                        ddlRole.Items.Add(new ListItem("Select", "0"));
                }
            }
            catch 
            {

                HttpContext.Current.ClearError();
            }
        }
        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            try
            {               
                string imgPath = "~/_layouts/CustomLoginPage/ProfilePictures/" + fupProfilePicture.PostedFile.FileName;
                DateTime createdDate = DateTime.Now;
                obj.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
                obj.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = txtUserName.Text;
                obj.cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = txtConfirmPwd.Text;
                obj.cmd.Parameters.Add("@PasswordSalt", SqlDbType.VarChar).Value = "";
                obj.cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = txtEmail.Text;
                //obj.cmd.Parameters.Add("@PasswordQuestion", SqlDbType.VarChar).Value = txtSecurityQstn.Text;
                //obj.cmd.Parameters.Add("@PasswordAnswer", SqlDbType.VarChar).Value = txtSecurityAnswer.Text;
                obj.cmd.Parameters.Add("@IsApproved", SqlDbType.Bit).Value = chkIsApproved.Checked;
                obj.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
                obj.cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = createdDate;
                obj.cmd.Parameters.Add("@UniqueEmail", SqlDbType.Int).Value = 0;
                obj.cmd.Parameters.Add("@PasswordFormat", SqlDbType.Int).Value = 0;
                obj.cmd.Parameters.Add("@MobilePhone", SqlDbType.VarChar).Value = txtMobilePhone.Text;
                obj.cmd.Parameters.Add("@ProfilePicture", SqlDbType.VarChar).Value = imgPath;
                obj.cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = txtFirstName.Text;
                obj.cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = txtLastName.Text;
                obj.cmd.Parameters.Add("@WorkPhone", SqlDbType.VarChar).Value = txtWorkPhone.Text;
                obj.cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                if (obj.exequery("sp_CreateUser"))
                {
                    obj1.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
                    obj1.cmd.Parameters.Add("@UserNames", SqlDbType.VarChar).Value = txtUserName.Text;
                    obj1.cmd.Parameters.Add("@RoleNames", SqlDbType.VarChar).Value = ddlRole.SelectedItem.Text;
                    obj1.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
                    if (obj1.exequery("aspnet_UsersInRoles_AddUsersToRoles"))
                    {
                        if (fupProfilePicture.HasFile)
                        {
                            fupProfilePicture.SaveAs(Server.MapPath(imgPath));
                        }
                        lblMsg.Text = "New user is successfully registered";
                        clearControls();
                    }
                }
            }
            catch
            {
                lblMsg.Text = "Try again later !!!!!";
            }
        }
        public void clearControls()
        {
            txtUserName.Text = "";
            txtPassWord.Text = "";
            txtEmail.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtMobilePhone.Text = "";
            txtConfirmPwd.Text = "";
            txtWorkPhone.Text = "";
        }
    }
}
