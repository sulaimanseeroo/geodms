﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LandingPage.aspx.cs" Inherits="CustomLoginPage.Layouts.CustomLoginPage.LandingPage"%>
<html>
<head>
<meta charset="utf-8">
<title>PSS - DMS</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="faveicon.png" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/view_port.css" rel="stylesheet" type="text/css">

<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/slide.css" type="text/css" media="screen" />
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700' rel='stylesheet' type='text/css'>


		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.roundabout.js"></script>
		<script type="text/javascript">
		    // <[CDATA[
		    $(document).ready(function () {
		        var interval;

		        $('.shadow ul')
					.roundabout(


					)
					.hover(
						function () {
						    // oh no, it's the cops!
						    clearInterval(interval);
						},
						function () {
						    // false alarm: PARTY!
						    interval = startAutoPlay();
						}
					);

		        // let's get this party started
		        interval = startAutoPlay();
		    });

		    function startAutoPlay() {
		        return setInterval(function () {
		            $('.shadow ul').roundabout_animateToNextChild();
		        }, 1500);
		        // top example
		    }
		    // ]]>
			
		</script>
		<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>


 <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    


</head>

<body>
<form runat="server">
<section id="section_top">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="user_details">
<div class="date_sec"><iframe src="http://free.timeanddate.com/clock/i3tjq0us/n861/fs16/fc747474/tcf2f1f1/pcf2f1f1/tt1/tw0/tm1" frameborder="0" width="97" height="21"></iframe>
</div>
<div class="user_name"><asp:Label ID="lblUsername" runat="server"></asp:Label></div>
</div>
</div>
</div>
</div>

</section>
<section id="section_header">
   <div class="logo"><img src="images/logo.png" width="86" height="128" alt=""/></div>

<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="main_tittle"><h1>Welcome to PSS - DMS </h1></div>
</div>
</div>
</div>
</section>
<section id="section_banner">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="shadow">
                  <ul style="margin:0; padding:0">
            <li><a href="#"><img src="images/banner.jpg" alt=""/></a></li>
            <li><a href="#"><img src="images/banner1.jpg" alt=""/></a></li>
            <li><a href="#"><img src="images/banner2.jpg" alt=""/></a></li>
            <li><a href="#"><img src="images/banner3.jpg" alt=""/></a></li>
          </ul>
              <div class="clear"></div>
                </div>
                
  
 <ul class="mob_privew">
            <li><a href="#"><img src="images/banner.jpg" width="100%" height="auto" alt=""/></a></li>
            <li><a href="#"><img src="images/banner1.jpg" width="100%" height="auto" alt=""/></a></li>
            <li><a href="#"><img src="images/banner2.jpg" width="100%" height="auto" alt=""/></a></li>
            <li><a href="#"><img src="images/banner3.jpg" width="100%" height="auto" alt=""/></a></li>
 </ul>
  
</div>
</div>
</div>
</section>
<section id="section_content">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="content_inner"><p>
The 18 cities and county government are SANDAG, the San Diego Association of Governments. This public agency serves as the forum for regional decision-making. SANDAG builds consensus; makes strategic plans; obtains and allocates resources; plans, engineers, and builds public transportation, and provides information on a broad range of topics pertinent to the region's quality of life.
</p></div>
</div>
</div>
</div>
</section>
<section id="login_footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
     <a class="toggleMenu" href="#">Menu</a>
          <ul class="nav">
            <li><a href="#">Company Info </a></li>
            <li><a href="#">About Us </a></li>
            <li><a href="#">Product Info</a></li>
            <li><a href="#">Customer Service</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li class="last_child"><a href="#">Testimonials</a></li>
          </ul>
      </div>
    </div>
  </div>
</section>
<footer id="footer_sec">

<div class="container">
    <div class="row">
      <div class="col-lg-12">
     <div class="copy_right"><p>© 2013  PSS - DMS. All Rights Reserved. </p></div>
     <ul class="footer_menu">
     <li  class="first_child"><a href="#">Help</a></li>
     <li><a href="#">E - mail Admin</a></li>
     </ul> 
    </div>
    </div>
  </div>
</footer>
<!--------------- toogle menu script --------------->
<script type="text/javascript" src="js/script.js"></script>
</form>
</body>
</html>
