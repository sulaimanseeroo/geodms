﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace CustomLoginPage.Layouts.CustomLoginPage
{
    public partial class LandingPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblUsername.Text = Session["username"].ToString();
        }
    }
}
