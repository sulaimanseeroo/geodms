﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.IdentityModel;
using Microsoft.SharePoint.IdentityModel.Pages;
using System.Text.RegularExpressions;
using System.Web.Security;
using System.Web;
using System.Text;
using System.Data;
using System.Configuration;
using System.Net.Mail;
using Microsoft.SharePoint.Administration.Claims;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Administration;

namespace CustomLoginPage.Layouts.CustomLoginPage
{
    public partial class LoginPage : System.Web.UI.Page
    {
        ConnectionClass obj = new ConnectionClass();
        ConnectionClass obj1 = new ConnectionClass();
        ConnectionClass obj2 = new ConnectionClass();
        ConnectionClass obj3 = new ConnectionClass();
        ConnectionClass obj4 = new ConnectionClass();
        public static DateTime createdDate;

          //borrowed from Microsoft.SharePoint.Utilities.SPUtility 
        private string EnsureUrlSkipsFormsAuthModuleRedirection(string url, bool urlIsQueryStringOnly)
        {  
            if (!url.Contains("ReturnUrl="))
            {     
                if (urlIsQueryStringOnly)  
                {      
                    url = url + (string.IsNullOrEmpty(url) ? "" : "&");  
                }       
                else    
                {       
                    url = url + ((url.IndexOf('?') == -1) ? "?" : "&");
                }       
                url = url + "ReturnUrl=";   
            }    
            return url;
        }

        //borrowed from Microsoft.SharePoint.IdentityModel.LogonSelector
        private void RedirectToLoginPage(SPAuthenticationProvider provider)
        {
            string components = HttpContext.Current.Request.Url.GetComponents(UriComponents.Query, UriFormat.SafeUnescaped);
            string url = provider.AuthenticationRedirectionUrl.ToString();
            if (provider is SPWindowsAuthenticationProvider)
            {
                components = EnsureUrlSkipsFormsAuthModuleRedirection(components, true);
            }
            SPUtility.Redirect(url, SPRedirectFlags.Default, this.Context, components);
        }  

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                
                if (!IsPostBack)
                {
                    
                    createdDate = DateTime.Now;
                    if (Request.Cookies["Username"] != null)
                    {
                        txtUsername.Text = Decryptdata(Request.Cookies["Username"].Value);
                    }
                    if (Request.Cookies["Password"] != null)
                    {
                        //txtPassWord.Text=Request.Cookies["Password"].Value;
                        txtPassWord.Attributes.Add("value", Decryptdata(Request.Cookies["Password"].Value.ToString()));
                    }

                    if (Request.Cookies["Username"] != null)
                        chkRemember.Checked = true;
                    if (txtUsername.Text == "" )
                        chkRemember.Checked = false;
                }
            }
            catch
            {

                HttpContext.Current.ClearError();
            }
        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {


            try
            {
                if ((txtUsername.Text.Length > 0 && txtPassWord.Text.Length > 0))
                {
                    bool authenticated = SPClaimsUtility.AuthenticateFormsUser(Context.Request.UrlReferrer, txtUsername.Text, txtPassWord.Text);
                    if (!authenticated)
                    {
                        obj3.cmd.Parameters.Add("@uname", SqlDbType.VarChar).Value = txtUsername.Text;
                        DataTable islocked = obj3.ViewData("aspnet_Membership_IsLockedOrNot");
                        if (islocked.Rows.Count > 0)
                        {
                            if (islocked.Rows[0]["IsLockedOut"].ToString().ToLower() == "true")
                            {
                                string TempPwd = GeneratePassword();
                                obj4.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
                                obj4.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = txtUsername.Text;
                                obj4.cmd.Parameters.Add("@TempPwd", SqlDbType.VarChar).Value = TempPwd;
                                if (obj4.exequery("aspnet_Membership_UnlockUser"))
                                {
                                    obj2.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
                                    obj2.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = txtUsername.Text;
                                    obj2.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
                                    obj2.cmd.Parameters.Add("@UpdateLastActivity", SqlDbType.Bit).Value = 0;
                                    DataTable dtUserDetails = obj2.ViewData("aspnet_Membership_GetUserByName");
                                    SendMail(dtUserDetails.Rows[0]["Email"].ToString(), "PSSDMS Temporary Password", TempPwd);
                                    lblMsg.Text = "Your Account has been locked out.A temporary password has sent  to your email address by the Administrator.";
                                }
                            }
                            else
                                lblMsg.Text = "&nbsp;&nbsp;Invalid Username or Password";
                        }
                        else
                            lblMsg.Text = "&nbsp;&nbsp;User has no sufficient permission to login";
                    }
                    else
                    {
                        HttpContext.Current.Session["username"] = txtUsername.Text;
                        HttpCookie Visit = new HttpCookie("User");
                        Visit.Values.Add("User", "success");

                        Visit.Expires = DateTime.Now.AddDays(1);
                        Response.Cookies.Add(Visit);

                        if (chkRemember.Checked)
                        {
                            Response.Cookies["Username"].Value = Encryptdata(txtUsername.Text);
                            //Response.Cookies["Password"].Value =Encryptdata(txtPassWord.Text);
                            Response.Cookies["Username"].Expires.AddDays(7);
                            //Response.Cookies["Password"].Expires.AddDays(7);                   
                        }
                        else
                        {
                            Response.Cookies["Username"].Value = null;
                            Response.Cookies["Password"].Value = null;
                        }
                        if (txtUsername.Text == "" )
                            chkRemember.Checked = false;
                        obj.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = txtUsername.Text;

                        DataTable dtCheckRole = obj.ViewData("CheckUserRole");
                        if (dtCheckRole.Rows.Count > 0)
                        {
                            if (dtCheckRole.Rows[0]["IsAdmin"].ToString().ToUpper() == "TRUE")
                            {
                                Response.Redirect("~/_layouts/CustomLoginPage/IndexPage.aspx");
                            }
                            else
                            {
                                obj1.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = txtUsername.Text;
                                DataTable dtSecurityQstn = obj1.ViewData("aspnet_Membership_CheckFirstLogin");
                                if (dtSecurityQstn.Rows.Count > 0)
                                {
                                    if (dtSecurityQstn.Rows[0]["IsFirstLogin"].ToString().ToLower() == "true")
                                    {
                                        Response.Redirect("~/_layouts/CustomLoginPage/EditUserProfile.aspx");
                                    }
                                    else
                                    {
                                        DateTime createdDate = DateTime.Now;
                                        string userName = Session["username"].ToString();
                                        obj2.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
                                        obj2.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = txtUsername.Text;
                                        obj2.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
                                        obj2.cmd.Parameters.Add("@UpdateLastActivity", SqlDbType.Bit).Value = 0;
                                        DataTable dtUserDetails = obj2.ViewData("aspnet_Membership_GetUserByName");
                                        if (dtUserDetails.Rows.Count > 0)
                                        {
                                            if (dtUserDetails.Rows[0]["IsPasswordChanged"].ToString().ToLower() == "true")
                                                Response.Redirect("~/_layouts/CustomLoginPage/ResetPassword.aspx");
                                            else
                                                Response.Redirect("~/_layouts/CustomLoginPage/IndexPage.aspx");
                                        }
                                    }
                                }
                            }

                            //Response.Redirect(Context.Request.QueryString["ReturnUrl"].ToString()); 
                        }
                        else
                        {
                            lblMsg.Text = "&nbsp;&nbsp;Username or Password can't be empty";
                        }
                    }
                }
            }
            catch
            {
                //lblMsg.Text = "Please try again!!!!";
                throw;

            }
        }
        private string Encryptdata(string password)
        {
            try
            {
                string strmsg = string.Empty;
                byte[] encode = new byte[password.Length];
                encode = Encoding.UTF8.GetBytes(password);
                strmsg = Convert.ToBase64String(encode);
                return strmsg;
            }
            catch
            {

                HttpContext.Current.ClearError();
                return null;
            }
        }
        private string Decryptdata(string encryptpwd)
        {
            try
            {
                string decryptpwd = string.Empty;
                UTF8Encoding encodepwd = new UTF8Encoding();
                Decoder Decode = encodepwd.GetDecoder();
                byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
                int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                decryptpwd = new String(decoded_char);
                return decryptpwd;
            }
            catch
            {

                HttpContext.Current.ClearError();
                return null;
            }
        }
        
        public string GeneratePassword()
        {
            try
            {
                string PasswordLength = "8";
                string allowedChars = "";
                allowedChars = "1,2,3,4,5,6,7,8,9,0";
                allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
                allowedChars += "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
                allowedChars += "~,!,@,#,$,%,^,&,*,+,?";

                char[] sep = { ',' };
                string[] arr = allowedChars.Split(sep);

                string IDString = "";
                string temp = "";

                Random rand = new Random();

                for (int i = 0; i < Convert.ToInt32(PasswordLength); i++)
                {
                    temp = arr[rand.Next(0, arr.Length)];
                    IDString += temp;


                }

                return IDString;
            }
            catch
            {

                HttpContext.Current.ClearError();
                return null;
            }
        }
        public static bool SendMail(string toMailAddress, string subject, string body)
        {
            bool isSent = false;

            try
            {
                //bool isServer = false;
                //bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["IsServer"], out isServer);


                //string mailFrom = "test@seeroo.com";
                //System.Configuration.ConfigurationManager.AppSettings.Get("MailFrom");
                string mailFrom = ConfigurationSettings.AppSettings["mailFrom"];
                // string mailDisplayName = "PSS-DMS";
                //System.Configuration.ConfigurationManager.AppSettings.Get("MailDisplayName");
                string mailDisplayName = ConfigurationSettings.AppSettings["mailDisplayName"];
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

                //checking if the mail form is not null and not an empty string
                if (!string.IsNullOrEmpty(mailFrom))
                {
                    //setting the dispaly name after null checking
                    msg.From = String.IsNullOrEmpty(mailDisplayName) ? new MailAddress(mailFrom) : new MailAddress(mailFrom, mailDisplayName);
                }

                //checking if the subject is not null and not an empty string
                if (!string.IsNullOrEmpty(subject))
                {
                    msg.Subject = subject;
                }

                msg.To.Add(toMailAddress);
                msg.Body = "Your temperory password is : " + body;
                msg.IsBodyHtml = true;

                System.Net.Mail.SmtpClient mailSender = new System.Net.Mail.SmtpClient();

                //string mailHost = "mail.seeroo.com";
                //System.Configuration.ConfigurationManager.AppSettings.Get("MailHost");
                string mailHost = ConfigurationSettings.AppSettings["mailHost"];
                //string mailPassword = "heLYpMLB";
                //System.Configuration.ConfigurationManager.AppSettings.Get("MailPassword");
                string mailPassword = ConfigurationSettings.AppSettings["mailPassword"];

                //setting credential if password and the mail from is not null
                if (!String.IsNullOrEmpty(mailFrom) && !String.IsNullOrEmpty(mailPassword))
                {
                    mailSender.Credentials = new System.Net.NetworkCredential(mailFrom, mailPassword);
                }
                //setting the host if the config host value is not null
                if (!String.IsNullOrEmpty(mailHost))
                {
                    mailSender.Host = mailHost;
                }

                mailSender.Send(msg);
                isSent = true;
            }
            catch (Exception ex)
            {
                isSent = false;
            }
            return isSent;
        }
        protected void btnForgotPwd_Click(object sender, EventArgs e) 
           {
               bool authenticated = SPClaimsUtility.AuthenticateFormsUser(Context.Request.UrlReferrer, "administrator", "Psqu@re1");
               if(authenticated)
               Response.Redirect("forgotpassword.aspx");
           }
        protected void btnForgotUsrname_Click(object sender, EventArgs e) 
           {
               bool authenticated = SPClaimsUtility.AuthenticateFormsUser(Context.Request.UrlReferrer, "administrator", "Psqu@re1");
               if(authenticated)
               Response.Redirect("ForgotUsername.aspx");           }

        protected void btnEmailToAdmin_Click(object sender, EventArgs e)
        {
            bool authenticated = SPClaimsUtility.AuthenticateFormsUser(Context.Request.UrlReferrer, "administrator", "Psqu@re1");
            if (authenticated)
            {               
                    hdfld.InnerText = "true";
                
            }
        }        
        
    }
    }


