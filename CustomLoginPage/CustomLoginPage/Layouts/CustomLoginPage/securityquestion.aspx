﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="securityquestion.aspx.cs"
    Inherits="CustomLoginPage.Layouts.CustomLoginPage.securityquestion" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Security Question | PSS - DMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="faveicon.png" type="image/x-icon" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/view_port.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700'
        rel='stylesheet' type='text/css'>    
    <link href="/_layouts/CustomLoginPage/css/bvalidator/bvalidator.css" rel="stylesheet" type="text/css" />
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery-latest.min.js" type="text/javascript"></script>
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery.bvalidator.js" type="text/javascript"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form1').bValidator();
    });
</script> 

<body class="inner_wrappermain">
    <form runat="server" id="form1">
    <div class="logo">
        <img src="images/logo.png" width="64" height="96" alt="" /></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="email_login">
                    <div class="email_body">
                        <%--<div class="logosub">
                        <img src="images/logo.png" width="100%" height="auto" /></div>--%>
                        <div class="email_temporary">
                            <h3>
                                Please Answer Your Security Question</h3>
                            <h5>
                                These Questions help us to know your Identity</h5>
                            <%--<p class="whatwas">What was your Childhood Nickname? </p>--%>
                            <%--<input class="user_box" type="text" value="Answer" name="user id">--%>
                            <p class="whatis">
                                <asp:Label ID="lblSecurityQstn" CssClass="whatwas" runat="server"></asp:Label>
                            <asp:TextBox class="user_box" runat="server" ID="txtPasswordAnswer" data-bvalidator="required"></asp:TextBox></p>
                            <%--<asp:RequiredFieldValidator ID="rfv_txtPasswordAnswer" ControlToValidate="txtPasswordAnswer"
                                runat="server" ErrorMessage="Enter your answer" Display="Dynamic" ValidationGroup="a"></asp:RequiredFieldValidator>--%>
                            <p class="forgot">
                                <asp:Label ID="lblPwdAnswer" CssClass="whatwas" runat="server" ForeColor="#FF3300"></asp:Label></p>
                            <p class="whatis">
                                <%-- <input class="user_box" type="text" value="Answer" name="user id">--%>
                                <%--<asp:TextBox class="user_box" runat="server" value="Answer" ID="user"></asp:TextBox>--%>
                                <asp:Label ID="lblSecurityQstn1" CssClass="whatwas" runat="server"></asp:Label>
                            <asp:TextBox class="user_box" runat="server" ID="txtPasswordAnswer1" data-bvalidator="required"></asp:TextBox></p>
                            <%--<asp:RequiredFieldValidator ID="rfv_txtPasswordAnswer1" ControlToValidate="txtPasswordAnswer1"
                                runat="server" ErrorMessage="Enter your answer" Display="Dynamic" ValidationGroup="a"></asp:RequiredFieldValidator>--%>
                            <p class="forgot">
                                <asp:Label ID="lblMsg" CssClass="whatwas" runat="server" ForeColor="#FF3300"></asp:Label></p>
                            <div class="clearfix">
                            </div>
                            <div class="back_btn">
                                <a href="/_layouts/CustomLoginPage/forgotpassword.aspx">Back</a></div>
                            <asp:Button ID="btnSubmit" ValidationGroup="a" CssClass="continue_btn" runat="server"
                                OnClick="btnSubmit_Click" Text="Submit" />
                        </div>
                    </div>
                </div>
                <div id="part_off">
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
