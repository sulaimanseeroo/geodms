﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IndexPage.aspx.cs" Inherits="CustomLoginPage.Layouts.CustomLoginPage.IndexPage" %>

<html>
<head>
    <meta charset="utf-8">
    <title>Home | PSS - DMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="faveicon.png" type="image/x-icon" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/view_port.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/slide.css" type="text/css" media="screen" />
    <script src="http://code.jquery.com/jquery-1.7.1.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.20/jquery-ui.min.js"
        type="text/javascript"></script>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700'
        rel='stylesheet' type='text/css'>
    <%--<script type="text/javascript" src="/_layouts/CustomLoginPage/js/jquery-1.3.2.min.js"></script>--%>
    <%--<script src="/_layouts/CustomLoginPage/js/jquery-1.7.1.js" type="text/javascript"></script>--%>
    <script src="/_layouts/CustomLoginPage/js/jquery.simplemodal-1.4.4.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OpenModal(src, Isadmin) {

            if (Isadmin == 'false') {
                $.modal('<iframe src="' + src + '" height="auto" width="100%" style="border:0">', {
                    closeHTML: " <img src='images/close.jpg' alt='' class='btnclose' />",
                    containerCss: {
                        backgroundColor: "#fff",
                        borderColor: "#fff",
                        height: 560,
                        padding: 2,
                        WebkitBorderTopLeftRadius: 15,
                        WebkitBorderTopRightRadius: 15,
                        WebkitBorderBottomLeftRadius: 15,
                        WebkitBorderBottomRightRadius: 15,
                        MozBorderRadius: 15,
                        BorderRadius: 15,
                        width: '40%',
                        closeByDocument: false,
                        overlayClose: false
                    },

                    overlayCss: { backgroundColor: "#666" }
                });
                return false;
            }


            function CloseModel() {
                $.modal.close();
            }
        }
    </script>
    <script type="text/javascript">

        function popup(url) {
            var NWin = window.open(url, '', 'scrollbars=1,height=600,width=800');
            if (window.focus) {
                NWin.focus();
            }
            return false;

        }
    </script>
    <style type="text/css">
    .email_login_new
    {
        top:224px!important;
        }
    </style>
    <%-- <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>--%>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
     <%--fancy box --%>
    <script type="text/javascript" src="/_layouts/CustomLoginPage/js/fancybox/jquery-1.10.1.min.js"></script>

	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="/_layouts/CustomLoginPage/js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="/_layouts/CustomLoginPage/js/fancybox/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="css/fancyboxcss/jquery.fancybox.css?v=2.1.5" media="screen" />
   
	<!-- Add Button helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="css/fancyboxcss/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="/_layouts/CustomLoginPage/js/fancybox/jquery.fancybox-buttons.js?v=1.0.5"></script>

	<!-- Add Thumbnail helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="css/fancyboxcss/jquery.fancybox-thumbs.css?v=1.0.7" />
	<script type="text/javascript" src="/_layouts/CustomLoginPage/js/fancybox/jquery.fancybox-thumbs.js?v=1.0.7"></script>

	<!-- Add Media helper (this is optional) -->
	<script type="text/javascript" src="/_layouts/CustomLoginPage/js/fancybox/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            /*
            *  Simple image gallery. Uses default settings
            */

            $('.fancybox').fancybox();

            /*
            *  Different effects
            */

            // Change title type, overlay closing speed
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    overlay: {
                        speedOut: 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                helpers: {
                    title: {
                        type: 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS: 'fancybox-custom',
                closeClick: true,

                openEffect: 'none',

                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay: {
                        css: {
                            'background': 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });

            // Remove padding, set opening and closing animations, close if clicked and disable overlay
            $(".fancybox-effects-d").fancybox({
                padding: 0,

                openEffect: 'elastic',
                openSpeed: 150,

                closeEffect: 'elastic',
                closeSpeed: 150,

                closeClick: true,

                helpers: {
                    overlay: null
                }
            });

            /*
            *  Button helper. Disable animations, hide close button, change title type and content
            */

            $('.fancybox-buttons').fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                prevEffect: 'none',
                nextEffect: 'none',

                closeBtn: false,

                helpers: {
                    title: {
                        type: 'inside'
                    },
                    buttons: {}
                },

                afterLoad: function () {
                    this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
                }
            });


            /*
            *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
            */

            $('.fancybox-thumbs').fancybox({
                prevEffect: 'none',
                nextEffect: 'none',

                closeBtn: false,
                arrows: false,
                nextClick: true,

                helpers: {
                    thumbs: {
                        width: 50,
                        height: 50
                    }
                }
            });

            /*
            *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
            */
            $('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
				    openEffect: 'none',
				    closeEffect: 'none',
				    prevEffect: 'none',
				    nextEffect: 'none',

				    arrows: false,
				    helpers: {
				        media: {},
				        buttons: {}
				    }
				});

            /*
            *  Open manually
            */

            $("#fancybox-manual-a").click(function () {
                $.fancybox.open('1_b.jpg');
            });

            $("#fancybox-manual-b").click(function () {
                $.fancybox.open({
                    href: 'iframe.html',
                    type: 'iframe',
                    padding: 5
                });
            });

            $("#fancybox-manual-c").click(function () {
                $.fancybox.open([
					{
					    href: '1_b.jpg',
					    title: 'My title'
					}, {
					    href: '2_b.jpg',
					    title: '2nd title'
					}, {
					    href: '3_b.jpg'
					}
				], {
				    helpers: {
				        thumbs: {
				            width: 75,
				            height: 50
				        }
				    }
				});
            });


        });
	</script>
    <style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}

		body {
			/*max-width: 700px;*/
			margin: 0 auto;
		}
	</style>
</head>
<body>
    <form runat="server">
    <section id="section_top">
  <div class="logo"><img src="images/logo.png" width="64" height="96" alt=""/></div>
  <div class="user_details">
          <div class="user_name"> <span class="user">
          <asp:Image ID="imgProfilePic" Width="100%" Height="30px" runat="server"></asp:Image>
          <%--<img src="images/person.jpg" width="100%" height="auto" alt=""/>--%></span>         
            <div class="name_detail"> 
            <span class="user_profile">
             <asp:Label ID="lblUsername"  CssClass="user_profile" runat="server" Text=""></asp:Label>                     
            </span>   
             <span class="user_edit">
             <asp:HyperLink ID="hypEdit" CssClass="fancybox fancybox.iframe" NavigateUrl="~/_layouts/CustomLoginPage/EditUserProfileForPopUp.aspx"  runat="server" Text="My Profile" ></asp:HyperLink>
             <%--<asp:LinkButton ID="lnkEdit"  runat="server">My Profile</asp:LinkButton>--%>
             <%--<asp:Button ID="btnEdit" runat="server" Text="My Profile"></asp:Button>--%>
             <%--<a id="lnkEdit" href="javascript:void(0);" onclick="OpenModal('EditUserProfileForPopUp.aspx','false');">My Profile</a>--%>
             <%--<asp:Button ID="lnkEdit" Text="My Profile" runat="server"></asp:Button>--%>
             </span>  
            <span class="last_login">            
            <asp:Label ID="lbllastlogin" CssClass="last_login" runat="server"></asp:Label>
            </span>
             </div>
             <div class="add_userin" runat="server" id="divAddUser">
            
             <%--<asp:LinkButton ID="lnkAddUser"  runat="server" OnClientClick="return OpenModal('/_layouts/CustomLoginPage/AddUser.aspx','false');return false;">Add User</asp:LinkButton>--%>
             <%--<asp:Button ID="btnAddUser" OnClientClick="return OpenModal('/_layouts/CustomLoginPage/AddUser.aspx','false');return false;" runat="server" Text="Add User"></asp:Button>--%>
             <%--<a id="newuser" href="javascript:void(0);" runat="server" onclick="return OpenModal('/_layouts/CustomLoginPage/AddUser.aspx','false');return false;">Add User</a>--%>
             <asp:HyperLink ID="hypAddUser" CssClass="fancybox fancybox.iframe" NavigateUrl="~/_layouts/CustomLoginPage/AddUser.aspx"  runat="server" Text="Add User" ></asp:HyperLink>
              <div class="clear"></div>
            </div>
                        
            <div class="sign_outsec">
            <asp:Button ID="btnLogOut" runat="server" OnClick="btnLogOut_Click" Text="Log Out"></asp:Button>
            <%--<asp:LinkButton ID="lnkLogout" OnClick="lnkLogout_Click" runat="server">Log Out</asp:LinkButton>--%></div>
          </div>
         
        </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="main_tittle"> 
          <h1>Welcome to PSS - DMS </h1>
        </div>
      </div>
      
    </div>
  </div>
</section>
    <section id="section_banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
<!--        <div class="dash_bord"><img src="images/dashbord_frame.png" width="99%" height="auto" alt="" align="center"/></div> -->  
      <div class="myFlipList">
          <div class="live-tile first_grid"> <a href="/PSSDMSDOCUMENT/Forms/AllItems.aspx"><img src="images/pix_01.jpg" width="100%" height="auto"/></a> </div>
          <div class="live-tile "> <a href="/SiteAssets/Splash.aspx"><img src="images/pix_02.jpg" width="100%" height="auto"/></a> </div>
          <div class="live-tile"> <a href="/PSSDMSDOCUMENT/Forms/pssdocument.aspx"><img src="images/pix_03.jpg" width="100%" height="auto"/></a> </div>
          <div class="live-tile"> <a href="/SiteAssets/advancedsearch.aspx"><img src="images/pix_04.jpg" width="100%" height="auto"/></a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
    <section id="section_content">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="content_inner">
          <h1>PSS - DMS</h1>
          <p> There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in 
            some form, by injected humour, or randomised words which don't look even slightly believable.
            If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing 
            hidden in the middle of text. There many variations of passages of Lorem Ipsum available, but the majority 
            have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. </p>
          <p> There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in 
            some form, by injected humour, or randomised words which don't look even slightly believable.
            If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing 
            hidden in the middle of text. There many variations of passages of Lorem Ipsum available, but the majority 
            have suffered alteration.</p>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="features_panel"> <img src="images/featurese_chart.png" width="100%" height="auto" alt="" align="right"/> </div>
      </div>
    </div>
  </div>
</section>
    <footer id="footer_sec">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="copy_right">
          <p>© 2013  PSS - DMS. All Rights Reserved. </p>
        </div>
        <div class="social_mediasec">
          <div class="sm_icn"><a href="#"><img src="images/facebook.jpg" alt=""/></a></div>
          <div class="sm_icn"><a href="#"><img src="images/twitter.jpg" alt=""/></a></div>
          <div class="sm_icn"><a href="#"><img src="images/linkedin.jpg" alt=""/></a></div>
          <div class="sm_icn"><a href="#"><img src="images/skype.jpg" alt=""/></a></div>
        </div>
        <ul class="footer_menu">
          <li  class="first_child"><a href="#" onclick="popup('/_layouts/PSS DMS INDEX HELP/index.html');" id="helpIndex" >Help</a> </li>
          <li><%--<asp:LinkButton ID="lnkEmailToAdmin" OnClientClick="return OpenModal('/_layouts/CustomLoginPage/EmailToAdmin.aspx','false');return false;" runat="server">Email Admin</asp:LinkButton>--%>
          <asp:HyperLink ID="hypEmailToAdmin" CssClass="fancybox fancybox.iframe" NavigateUrl="~/_layouts/CustomLoginPage/EmailToAdmin.aspx"  runat="server" Text="Email Admin" ></asp:HyperLink>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>
    <!--------------- toogle menu script --------------->
    <script src="js/metrojs.js" type="text/javascript"></script>
    <%--<script type="text/javascript">
    // FlipList triggerDelay
    $("#myFlipList").liveTile({
        tileSelector: '>div:not(.exclude)',
        alwaysTrigger: true
//        triggerDelay: function (idx) {
//            return idx * 650;
//        }
    });
    </script> --%>
    <script type="text/javascript" src="js/script.js"></script>
    </form>
</body>
</html>
