﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.IdentityModel;
using Microsoft.SharePoint.IdentityModel.Pages;
using System.Text.RegularExpressions;
using System.Web.Security;

namespace CustomLoginPage.Layouts.CustomLoginPage
{
    public partial class Login : System.Web.UI.Page
    {         
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            bool status = SPClaimsUtility.AuthenticateFormsUser(Context.Request.UrlReferrer, TextBox1.Text, TextBox2.Text);
            if (!status)
            {
                Label1.Text = "Wrong Userid or Password";
            }
            else
            {
                if (Context.Request.QueryString.Keys.Count > 1)
                {
                    Response.Redirect(Context.Request.QueryString["Source"].ToString());
                }
                else
                    Response.Redirect(Context.Request.QueryString["ReturnUrl"].ToString());
            }
        }

        protected void ChangePasswordBtn_Click(object sender, EventArgs e)
        {
            string Username = txtUname.Text;
            string pass = ConfirmNewPassword.Text;
            MembershipUser mu = Membership.GetUser(Username);
            mu.ChangePassword(mu.ResetPassword(), pass);
        }

 
    }
}
