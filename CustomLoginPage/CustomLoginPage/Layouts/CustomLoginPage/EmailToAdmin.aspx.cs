﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Data;
using System.Net.Mail;
using System.Configuration;
using System.Web;
using System.Text;

namespace CustomLoginPage.Layouts.CustomLoginPage
{
    public partial class EmailToAdmin : LayoutsPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
        }
        public static bool SendMail(string FromMailAddress, string subject, string body)
        {
            bool isSent = false;

            try
            {
                //bool isServer = false;
                //bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["IsServer"], out isServer);


                string mailFrom = "test@seeroo.com";
                //System.Configuration.ConfigurationManager.AppSettings.Get("MailFrom");
                string mailDisplayName = "PSS-DMS";
                //System.Configuration.ConfigurationManager.AppSettings.Get("MailDisplayName");

                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

                //checking if the mail form is not null and not an empty string
                if (!string.IsNullOrEmpty(mailFrom))
                {
                    //setting the dispaly name after null checking
                    msg.From = String.IsNullOrEmpty(mailDisplayName) ? new MailAddress(mailFrom) : new MailAddress(mailFrom, mailDisplayName);
                }

                //checking if the subject is not null and not an empty string
                if (!string.IsNullOrEmpty(subject))
                {
                    msg.Subject = subject;
                }

                msg.To.Add(FromMailAddress);
                msg.Body = " " + body;
                msg.IsBodyHtml = true;

                System.Net.Mail.SmtpClient mailSender = new System.Net.Mail.SmtpClient();

                string mailHost = "mail.seeroo.com";
                //System.Configuration.ConfigurationManager.AppSettings.Get("MailHost");
                string mailPassword = "heLYpMLB";
                //System.Configuration.ConfigurationManager.AppSettings.Get("MailPassword");

                //setting credential if password and the mail from is not null
                if (!String.IsNullOrEmpty(mailFrom) && !String.IsNullOrEmpty(mailPassword))
                {
                    mailSender.Credentials = new System.Net.NetworkCredential(mailFrom, mailPassword);
                }
                //setting the host if the config host value is not null
                if (!String.IsNullOrEmpty(mailHost))
                {
                    mailSender.Host = mailHost;
                }

                mailSender.Send(msg);
                isSent = true;
            }
            catch (Exception ex)
            {
                isSent = false;
            }
            return isSent;
        }
        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                string AdminEmail = ConfigurationSettings.AppSettings["adminMail"];
                if(SendMail(AdminEmail,txtSubject.Text.ToString(),txtMesssage.Text.ToString()))
                {
                    txtMesssage.Text = "";
                    txtSubject.Text = "";
                    lblMsg.Text ="Your message has been sent successfully.";
                }
            }
            catch
            {
            }
        }
    }
}
