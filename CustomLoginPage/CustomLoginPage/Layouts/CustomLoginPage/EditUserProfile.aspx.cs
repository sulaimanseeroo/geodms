﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Data;
using System.Web;
using System.Configuration;

namespace CustomLoginPage.Layouts.CustomLoginPage
{
    public partial class EditUserProfile : LayoutsPageBase
    {
        ConnectionClass obj = new ConnectionClass();
        ConnectionClass obj1 = new ConnectionClass();
        ConnectionClass obj2 = new ConnectionClass();
        public static string userName;
        public static DateTime createdDate;        
        bool Isexist = false;
        public static string  isFirstLogin = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    userName = Session["username"].ToString();
                    createdDate = DateTime.Now;
                    BindEditDetails();
                    obj2.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
                    DataTable dtSecurityQstn = obj2.ViewData("aspnet_Membership_CheckFirstLogin");
                    if (dtSecurityQstn.Rows.Count > 0)
                    {
                        isFirstLogin = dtSecurityQstn.Rows[0]["IsFirstLogin"].ToString();
                        if (isFirstLogin.ToLower() == "true")
                        {
                            SecurityDiv.Visible = true;
                            BindSecurityQuestion();
                        }
                        else
                            SecurityDiv.Visible = false;
                    }

                }
            }
            catch 
            {

                HttpContext.Current.ClearError();
            }
        }
        private void BindEditDetails()
        {
            try
            {
                obj.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
                obj.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
                obj.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
                obj.cmd.Parameters.Add("@UpdateLastActivity", SqlDbType.Bit).Value = 0;
                DataTable EditUser = obj.ViewData("aspnet_Membership_GetUserByName");
                txtFirstName.Text = EditUser.Rows[0]["FirstName"].ToString();
                txtLastName.Text = EditUser.Rows[0]["LastName"].ToString();
                txtMobilePhone.Text = EditUser.Rows[0]["MobilePhone"].ToString();
                txtWorkPhone.Text = EditUser.Rows[0]["WorkPhone"].ToString();
                txtEmail.Text = EditUser.Rows[0]["Email"].ToString();
            }
            catch 
            {

                HttpContext.Current.ClearError();
            }
        }

        private void BindSecurityQuestion()
        {
            try
            {
                DataTable dtSecurityQuestions = obj1.ViewData("sp_GetSecurityQuestions");
                if (dtSecurityQuestions.Rows.Count > 0)
                {
                    lblSecurityQuestion1.Text = dtSecurityQuestions.Rows[0]["SecurityQuestion"].ToString();
                    lblSecurityQuestion2.Text = dtSecurityQuestions.Rows[1]["SecurityQuestion"].ToString();
                }
            }
            catch 
            {

                HttpContext.Current.ClearError();
            }
        }
        protected void btnEditUser_Click(object sender, EventArgs e)
        {
            try
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string ProfilePIc = "";
                if (fupProfilePicture.HasFile)
                {
                    ProfilePIc = "~/_layouts/CustomLoginPage/ProfilePictures/" + fupProfilePicture.PostedFile.FileName;
                }
                else
                {
                    if (!string.IsNullOrEmpty(Session["ProfilePic"] as string))

                    {
                        ProfilePIc = Session["ProfilePic"].ToString();
                    }
                }
                if (isFirstLogin.ToLower() == "true")
                {
                    obj.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
                    obj.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
                    obj.cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = txtEmail.Text;
                    obj.cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = txtFirstName.Text;
                    obj.cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = txtLastName.Text;
                    obj.cmd.Parameters.Add("@MobilePhone", SqlDbType.VarChar).Value = txtMobilePhone.Text;
                    obj.cmd.Parameters.Add("@WorkPhone", SqlDbType.VarChar).Value = txtWorkPhone.Text;
                    obj.cmd.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = createdDate;
                    obj.cmd.Parameters.Add("@LastActivityDate", SqlDbType.DateTime).Value = createdDate;
                    obj.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
                    obj.cmd.Parameters.Add("@SecurityQuestion1", SqlDbType.VarChar).Value = lblSecurityQuestion1.Text;
                    obj.cmd.Parameters.Add("@SecurityQuestion2", SqlDbType.VarChar).Value = lblSecurityQuestion2.Text;
                    obj.cmd.Parameters.Add("@SecurityAnswer1", SqlDbType.VarChar).Value = txtSecurityQuestion1.Text;
                    obj.cmd.Parameters.Add("@SecurityAnswer2", SqlDbType.VarChar).Value = txtSecurityQuestion2.Text;
                    obj.cmd.Parameters.Add("@flag", SqlDbType.Int).Value = 2;
                    obj.cmd.Parameters.Add("@isFirstLogin", SqlDbType.Bit).Value = 0;
                    obj.cmd.Parameters.Add("@ProfilePicture", SqlDbType.VarChar).Value = ProfilePIc;
                    if (obj.exequery("sp_UpdateUser"))
                    {
                        if (fupProfilePicture.HasFile)
                        {
                            fupProfilePicture.SaveAs(Server.MapPath(ProfilePIc));
                        }
                        Response.Redirect("IndexPage.aspx");
                       
                    }
                }
                else
                {
                    obj.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
                    obj.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
                    obj.cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = txtEmail.Text;
                    obj.cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = txtFirstName.Text;
                    obj.cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = txtLastName.Text;
                    obj.cmd.Parameters.Add("@MobilePhone", SqlDbType.VarChar).Value = txtMobilePhone.Text;
                    obj.cmd.Parameters.Add("@WorkPhone", SqlDbType.VarChar).Value = txtWorkPhone.Text;
                    obj.cmd.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = createdDate;
                    obj.cmd.Parameters.Add("@LastActivityDate", SqlDbType.DateTime).Value = createdDate;
                    obj.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
                    obj.cmd.Parameters.Add("@flag", SqlDbType.Int).Value = 1;
                    obj.cmd.Parameters.Add("@isFirstLogin", SqlDbType.Bit).Value = 0;
                    obj.cmd.Parameters.Add("@ProfilePicture", SqlDbType.VarChar).Value = ProfilePIc;
                    if (obj.exequery("sp_UpdateUser"))
                    {
                        if (fupProfilePicture.HasFile)
                        {
                            fupProfilePicture.SaveAs(Server.MapPath(ProfilePIc));
                        }
                        Response.Redirect("IndexPage.aspx");
                        //context.Response.Flush();
                        //context.Response.End();
                    }

                }
            }
            catch
            {

                lblMsg.Text = "Internal server error, Please check the connection";
            }        
        }
    }
}
