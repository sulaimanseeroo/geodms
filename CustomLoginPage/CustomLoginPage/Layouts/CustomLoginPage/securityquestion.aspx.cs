﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Data;
using System.Configuration;
using System.Text;
using System.Web;

namespace CustomLoginPage.Layouts.CustomLoginPage
{
    public partial class securityquestion : LayoutsPageBase
    {
        ConnectionClass obj = new ConnectionClass();
        public static string userName;
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            lblPwdAnswer.Text = "";
            if (!IsPostBack)
            {
                try
                {

                    DateTime createdDate = DateTime.Now;
                    string uname = Session["usrname"].ToString();
                    userName = Decryptdata(uname);
                    if (userName != "")
                    {
                        obj.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
                        obj.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
                        obj.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
                        obj.cmd.Parameters.Add("@UpdateLastActivity", SqlDbType.Bit).Value = 0;
                        DataTable securityQstn = obj.ViewData("aspnet_Membership_GetUserByName");
                        lblSecurityQstn.Text = securityQstn.Rows[0]["PasswordQuestion"].ToString();
                        lblSecurityQstn1.Text = securityQstn.Rows[0]["PasswordQuestion1"].ToString();
                    }
                }
                catch
                {


                }
            }
        }
        private string Decryptdata(string encryptpwd)
        {
            try
            {
                string decryptpwd = string.Empty;
                UTF8Encoding encodepwd = new UTF8Encoding();
                Decoder Decode = encodepwd.GetDecoder();
                byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
                int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                decryptpwd = new String(decoded_char);
                return decryptpwd;
            }
            catch
            {

                HttpContext.Current.ClearError();
                return null;
            }
        }
        private string Encryptdata(string password)
        {
            try
            {
                string strmsg = string.Empty;
                byte[] encode = new byte[password.Length];
                encode = Encoding.UTF8.GetBytes(password);
                strmsg = Convert.ToBase64String(encode);
                return strmsg;
            }
            catch
            {

                HttpContext.Current.ClearError();
                return null;
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                obj.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
                string PwdAnswer = txtPasswordAnswer.Text;
                string PwdAnswer1 = txtPasswordAnswer1.Text;
                DataTable CheckAnswer = obj.ViewData("sp_CheckPasswordAnswer");
                string PwdAnswerFrmDb = CheckAnswer.Rows[0]["PasswordAnswer"].ToString();
                string PwdAnswer1FrmDb = CheckAnswer.Rows[0]["PasswordAnswer1"].ToString();

                if (PwdAnswer != PwdAnswerFrmDb)
                {
                    lblPwdAnswer.Text = "Answer is wrong";
                }
                if (PwdAnswer1 != PwdAnswer1FrmDb)
                {
                    lblMsg.Text = "Answer is wrong";
                }
                if (PwdAnswer == PwdAnswerFrmDb && PwdAnswer1 == PwdAnswer1FrmDb)
                {
                    Response.Redirect("~/_layouts/CustomLoginPage/ResetPasswordPage.aspx");
                }

            }
            catch
            {
            }
        }
    }
}
