﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Data;
using System.Configuration;
using System.Web;
using System.Text;

namespace CustomLoginPage.Layouts.CustomLoginPage
{
    public partial class forgotpassword : LayoutsPageBase
    {
        ConnectionClass obj = new ConnectionClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            {
                rdbSecurityQuestion.Checked = true;
            }
        }
        protected void btncontinue_Click(object sender, EventArgs e)
        {
            try
            {
                string uname = userid.Text;
                string userName = Encryptdata(uname);
                bool isValid = CheckUserID(uname);
                if (isValid == true)
                {
                    if (rdbSecurityQuestion.Checked)
                    {
                        Session["usrname"] = userName;
                        Response.Redirect("~/_layouts/CustomLoginPage/securityquestion.aspx");
                    }
                    else if (rdbEmailTemporaryPassword.Checked)
                    {
                        Response.Redirect("~/_layouts/CustomLoginPage/emailtemperorypassword.aspx?userid=" + userName);
                    }
                    else
                    {
                        Response.Redirect("~/_layouts/CustomLoginPage/smstemperorypassword.aspx?userid=" + userName);
                    }
                }
                else
                {
                    lblMsg.Text = "Invalid UserID";                   
                }
            }
            catch 
            {
                
               
            }
        }

        public  bool CheckUserID(string userName)
        {
            try
            {
                DateTime createdDate = DateTime.Now;
                obj.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
                obj.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
                obj.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
                obj.cmd.Parameters.Add("@UpdateLastActivity", SqlDbType.Bit).Value = 0;
                DataTable dtCheckUser = obj.ViewData("aspnet_Membership_GetUserByName");
                if (dtCheckUser.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch 
            {
                return false;
            }
        }
        private string Encryptdata(string password)
        {
            try
            {
                string strmsg = string.Empty;
                byte[] encode = new byte[password.Length];
                encode = Encoding.UTF8.GetBytes(password);
                strmsg = Convert.ToBase64String(encode);
                return strmsg;
            }
            catch
            {

                HttpContext.Current.ClearError();
                return null;
            }
        }
        
        
    }
}
