﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Data;
using System.Web;
using System.Configuration;
using System.Text;

namespace CustomLoginPage.Layouts.CustomLoginPage
{
    public partial class ResetPasswordPage : System.Web.UI.Page
    {
        ConnectionClass obj = new ConnectionClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            txtNewPassword.Attributes.Add("OnKeyUp", "PasswordChanged(this)");           
        }       
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime createdDate = DateTime.Now;
                string userName = Decryptdata(Session["usrname"].ToString());
                obj.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
                obj.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
                obj.cmd.Parameters.Add("@NewPassword", SqlDbType.VarChar).Value = txtConfirmPassword.Text;
                obj.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
                if (obj.exequery("sp_ResetPassword"))
                {
                    //ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Your password is successfully reset');", true);
                    lblMsg.Text = "Your password is successfully reset";
                    Response.Redirect("LoginPage.aspx");
                    //lblMsg.Text = "Your password is successfully reset";

                }
                else
                    lblMsg.Text = "Internal server error!!!!Please try again later!!!";
            }
            catch 
            {

                HttpContext.Current.ClearError();
            }
        }
        private string Decryptdata(string encryptpwd)
        {
            try
            {
                string decryptpwd = string.Empty;
                UTF8Encoding encodepwd = new UTF8Encoding();
                Decoder Decode = encodepwd.GetDecoder();
                byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
                int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                decryptpwd = new String(decoded_char);
                return decryptpwd;
            }
            catch
            {

                HttpContext.Current.ClearError();
                return null;
            }
        }
    }
}
