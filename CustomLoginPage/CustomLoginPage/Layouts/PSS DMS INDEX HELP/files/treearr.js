var TITEMS = [ 
 ["Home", null, "1",
  ["PSS DMS Home", "pss dms home.htm", "11"],
  ["Mangae Documents", null, "1",
   ["What is Manage Document", "manage document.htm", "11"],
   ["Add Document", "add document.htm", "11"],
   ["Edit Properties", "edit properties.htm", "11"]
  ],
  ["Interactive Map", "interactive map.htm", "11"],
  ["Advanced Search", "advanced search.htm", "11"],
  ["My Documents", "my documents.htm", "11"],
  ["User Management", null, "1",
   ["New User", "new user.htm", "11"],
   ["Profile Management", "profile management.htm", "11"]
  ],
  ["Bulk Upload", "bulk upload.htm", "11"]
 ]
];


var FITEMS = arr_flatten(TITEMS);

function arr_flatten (x) {
   var y = []; if (x == null) return y;
   for (var i=0; i<x.length; i++) {
      if (typeof(x[i]) == "object") {
         var flat = arr_flatten(x[i]);
         for (var j=0; j<flat.length; j++)
             y[y.length]=flat[j];
      } else {
         if ((i%3==0))
          y[y.length]=x[i+1];
      }
   }
   return y;
}

