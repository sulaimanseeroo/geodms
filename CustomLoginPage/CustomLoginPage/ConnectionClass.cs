﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CustomLoginPage
{
    class ConnectionClass
    {
        #region Publicvariables

        public SqlConnection dbCon;

        //public SqlCommand objSqlCmd;
        public SqlCommand cmd = new SqlCommand();
        public SqlDataAdapter da = new SqlDataAdapter();
        public DataTable dt = new DataTable();
        public SqlDataReader dr;

        #endregion
        public void openConnection()
        {
            try
            {
                dbCon = new SqlConnection();
                string varCon = ConfigurationSettings.AppSettings["ConStr"];

                dbCon = new SqlConnection(varCon);

                if (dbCon.State == ConnectionState.Closed)
                {
                    dbCon.Open();
                }
                else
                {
                    dbCon.Close();
                }
            }

            catch (Exception Ex)
            {
                dbCon.Close();

                throw Ex;
            }
        }
        public DataTable ViewData(string query)
        {
            //cmd.Connection = getcon();
            openConnection();
            cmd.Connection = dbCon;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = query;
            da.SelectCommand = cmd;
            dt.Rows.Clear();
            da.Fill(dt);
            return dt;
        }
        public bool exequery(string spname)
        {
            try
            {
                openConnection();
                cmd.Connection = dbCon;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = spname;
                cmd.ExecuteNonQuery().ToString();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public SqlDataReader Reader(string spName)
        {
            openConnection();
            cmd.Connection = dbCon;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = spName;
            dr = cmd.ExecuteReader();
            return dr;
        }      

    }
}
