<%@ Assembly Name="CustomLoginPage, Version=1.0.0.0, Culture=neutral, PublicKeyToken=7247f84a56c4f5f6" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddUserUserControl.ascx.cs"
    Inherits="CustomLoginPage.AddUser.AddUserUserControl" %>
<head>
    <link rel="icon" href="/_layouts/CustomLoginPage/faveicon.png" type="image/x-icon" />
    <link href="/_layouts/CustomLoginPage/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/view_port.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700'
        rel='stylesheet' type='text/css' />
        <style>
        .email_login 
        {
            top:210px!important;
        }
        </style>
</head>
<body class="inner_wrappermain">
    <div id="wrap">       
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="email_login">
                        <div class="email_temporary">
                            <h1>
                                Add User</h1>
                            <h5>
                            </h5>
                            <p>
                                <span class="user_inn">First Name</span>
                                <asp:TextBox ID="txtFirstName" CssClass="user_innbox" runat="server"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Last Name</span>
                                <asp:TextBox ID="txtLastName" CssClass="user_innbox" runat="server"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Username<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                    runat="server" ControlToValidate="txtUserName" ErrorMessage="*"></asp:RequiredFieldValidator></span>
                                <asp:TextBox ID="txtUserName" CssClass="user_innbox" runat="server"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Password</span>
                                <asp:TextBox ID="txtPassWord" runat="server" CssClass="user_innbox" TextMode="Password"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Confirm Password</span>
                                <asp:TextBox ID="txtConfirmPwd" CssClass="user_innbox" runat="server" TextMode="Password"></asp:TextBox>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassWord"
                                    ControlToValidate="txtConfirmPwd" ErrorMessage="Password Missmatch"></asp:CompareValidator>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                           <%-- <p>
                                <span class="user_inn">Security Question<asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                    runat="server" ControlToValidate="txtSecurityQstn" ErrorMessage="*"></asp:RequiredFieldValidator></span>
                                <asp:TextBox ID="txtSecurityQstn" CssClass="user_innbox" runat="server" TextMode="MultiLine"></asp:TextBox>
                                <input class="user_innbox" type="text"
                                    value="" name="user id"></p>
                            <p>
                                <span class="user_inn">Answer<asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                    runat="server" ControlToValidate="txtSecurityAnswer" ErrorMessage="*"></asp:RequiredFieldValidator></span>
                                <asp:TextBox ID="txtSecurityAnswer" CssClass="user_innbox" runat="server"></asp:TextBox>
                                <input
                                        class="user_innbox" type="text" value="" name="user id"></p>--%>
                            <p>
                                <span class="user_inn">IsApproved</span>
                                <asp:CheckBox ID="chkIsApproved" runat="server" />
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Email<asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                    runat="server" ControlToValidate="txtEmail" ErrorMessage="*"></asp:RequiredFieldValidator></span>
                                <asp:TextBox ID="txtEmail" CssClass="user_innbox" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                                    ErrorMessage="Invalid Email ID" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Select Role</span>
                                <asp:DropDownList ID="ddlRole" CssClass="RoleDropdown" runat="server" Width="150px">
                                </asp:DropDownList>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Mobile Phone</span>
                                <asp:TextBox ID="txtMobilePhone" CssClass="user_innbox" runat="server"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Work Phone</span>
                                <asp:TextBox ID="txtWorkPhone" runat="server" CssClass="user_innbox"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Profile Picture</span>
                                <asp:FileUpload ID="fupProfilePicture" runat="server" />
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <div class="clearfix">
                            </div>
                            <%--<div class="back_btn">
                                <a href="forgot_password.html">Back</a></div>--%>
                            <asp:Button ID="btnAddUser" CssClass="continue_btn" runat="server" OnClick="btnAddUser_Click"
                                Text="Add User" />
                            <asp:Label ID="lblMsg" runat="server"></asp:Label>
                            <%--<a href="#" class="continue_btn">SUBMIT</a>--%>
                        </div>
                    </div>
                    <div id="part_off">
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
