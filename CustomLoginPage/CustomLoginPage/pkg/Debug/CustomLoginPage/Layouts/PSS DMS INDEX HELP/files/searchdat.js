var SearchFiles = ["add document.htm",
"advanced search.htm",
"bulk upload.htm",
"edit properties.htm",
"interactive map.htm",
"manage document.htm",
"my documents.htm",
"new user.htm",
"profile management.htm",
"pss dms home.htm"];

var SearchTitles = ["Add Document",
"Advanced Search",
"Bulk Upload",
"Edit Properties",
"Interactive Map",
"Manage Document",
"My Documents",
"New User",
"Profile Management",
"Home"];

var SearchIndexes = [["ABOVE",[1]],["ACCORDING",[1,9]],["ADD",[0,1,5,7,9]],["ADMIN",[7,9]],["ADMINISTRATOR",[7]],["ADVANCED",[1,9]],["AFTER",[2,3]],["AGAIN",[1]],["ALL",[1,6,9]],["ALLOWS",[1]],["ALSO",[5,9]],["AND",[0,1,2,3,4,5,7,9]],["ARE",[1,2,3,6,9]],["ARROW",[3]],["ATTRIBUTES",[1,5,9]],["AVAILABLE",[1]],["BELOW",[8]],["BOTTOM",[0]],["BOX",[1]],["BULK",[2]],["BUTTON",[1]],["CAN",[1,4,7,8,9]],["CAPABILITIES",[1]],["CHECK",[5,9]],["CHM",[0,1,2,3,4,5,6,7,8,9]],["CHM2WEB",[0,1,2,3,4,5,6,7,8,9]],["CLICK",[0,3]],["CLICKING",[1,8]],["COLUMNS",[1]],["CONTAINS",[1]],["CONTENT",[1]],["CONVERTED",[0,1,2,3,4,5,6,7,8,9]],["CORRESPONDING",[1]],["CREATE",[7]],["CREATING",[7,9]],["CRITERIA",[1]],["CURRENT",[6,9]],["DELETE",[5,9]],["DESIRED",[1]],["DESKTOP",[2]],["DETAILS",[1,7]],["DIFFERENT",[1]],["DMS",[9]],["DOCUMENT",[0,1,3,5,9]],["DOCUMENTS",[1,4,5,6,9]],["DOWN",[3]],["DOWNLOAD",[1]],["DRAG",[2]],["DROP",[2]],["EDIT",[3,5,8,9]],["EDITING",[3]],["EITHER",[1]],["ENOUGH",[1]],["ENTER",[7]],["ENTERING",[1]],["EQUALS",[1]],["EQUALS…",[1]],["FIELD",[1,3]],["FIGURE",[1,7]],["FILES",[2]],["FILTER",[1]],["FILTERING",[1,9]],["FIND",[1]],["FOR",[1,3,5,7,9]],["FROM",[0,1,2,3,4,5,6,7,8,9]],["FUNCTIONAL",[1]],["GIVE",[1]],["HAS",[7,9]],["HAVE",[1,5,9]],["HELP",[1]],["HELPS",[1,9]],["HIS",[8]],["HOME",[7,8,9]],["HTML",[0,1,2,3,4,5,6,7,8,9]],["INTERACTIVE",[4,9]],["INTO",[8]],["ITEM",[3]],["ITS",[1,9]],["LIBRARY",[2]],["LIKE",[8]],["LINK",[0,1,3,7,8]],["LIST",[1,5,6,9]],["LOCATED",[8]],["LOGIN",[9]],["LOGINS",[8]],["LOOK",[8]],["MANAGE",[5,9]],["MANAGEMENT",[8,9]],["MAP",[4,9]],["MEANINGFUL",[1]],["MEETING",[1]],["MENU",[3]],["MORE",[1]],["MULTIPLE",[1,2]],["NAME",[3]],["NARROW",[1]],["NEW",[0,7,9]],["NOT",[1]],["NUMBER",[1,5,9]],["ONE",[1]],["OPERATOR",[1]],["OPERATORS",[1]],["OPTION",[1]],["OPTIONS",[3,5,7,9]],["OUT",[5,9]],["PAGE",[0,1,7,8]],["PICK",[1]],["PLUS",[1]],["POINTS",[4,9]],["PRO",[0,1,2,3,4,5,6,7,8,9]],["PROFILE",[8,9]],["PROPERTIES",[0,1,3,9]],["PROPERTIES”",[3]],["PROPERTY",[1]],["PROVIDE",[1,9]],["PROVIDED",[1,9]],["PSS",[9]],["PSS_DMS",[7]],["RESTRICTIONS",[1]],["RESULT",[1,9]],["RESULTS",[1]],["RETRIEVE",[4,9]],["RIBBON",[3]],["SEARCH",[1,9]],["SEARCHES",[1]],["SEARCHING",[1]],["SEARCH”",[1,9]],["SECOND",[1]],["SECTIONS",[1]],["SELECT",[2]],["SELECTED",[1,4,9]],["SELECTING",[3]],["SET",[0]],["SHOW",[1,3]],["SHOWN",[1,7]],["SHOWS",[1]],["SITE",[8]],["SUBMIT",[7]],["SUCCESSFUL",[9]],["SUCH",[9]],["TAKE",[1]],["TEXT",[1]],["THAN",[1]],["THAT",[1,6,9]],["THE",[0,1,2,3,4,5,6,7,8,9]],["THERE",[1,3,7]],["THEY",[8]],["THIRD",[1]],["THIS",[1]],["TITLE",[1]],["TOP",[0,3]],["TWO",[1,3]],["UNICODE",[0,1,2,3,4,5,6,7,8,9]],["UPLOAD",[0,2,4,9]],["UPLOADED",[1,2,6,9]],["USE",[1]],["USED",[1]],["USER",[6,7,8,9]],["USERS",[1,4,9]],["VALUE",[1]],["VALUES",[1]],["VARIOUS",[1]],["VIEW",[1,8]],["WANT",[1]],["WHEN",[1]],["WILL",[1,3,7,8,9]],["WISE",[1]],["WITH",[0,1,2,3,4,5,6,7,8,9]],["“ADVANCED",[1,9]],["“AND”",[1]],["“EDIT",[3]],["“OR”",[1]]];