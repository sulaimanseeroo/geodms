<%@ Assembly Name="CustomLoginPage, Version=1.0.0.0, Culture=neutral, PublicKeyToken=7247f84a56c4f5f6" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPasswordForPopUp.aspx.cs"
    Inherits="CustomLoginPage.Layouts.CustomLoginPage.ResetPasswordForPopUp" %>

<html>
<head>
    <meta charset="utf-8">
    <title>PSS - DMS | Reset Password</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="faveicon.png" type="image/x-icon" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/view_port.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700'
        rel='stylesheet' type='text/css'>
    <link href="/_layouts/CustomLoginPage/css/bvalidator/bvalidator.css" rel="stylesheet"
        type="text/css" />
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery-latest.min.js" type="text/javascript"></script>
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery.bvalidator.js" type="text/javascript"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <style>
        .email_login
        {
            top: 280px !important;
        }
        #lblpwd2Msg
        {
            padding-left: 8px;
        }
    </style>
    <script type="text/javascript">
        var options = {
            offset: { x: 5, y: -2 },
            position: { x: 'left', y: 'top' }
        };
        $(document).ready(function () {
            $('#form1').bValidator(options);
        });
    </script>
    <script type="text/javascript">
        function PasswordChanged(field) {
            var pwd = document.getElementById("PasswordStrength");
            pwd.innerHTML = CheckPassword(field.value);
            if (pwd.innerHTML == "Very Strong")
                pwd.style.color = "Green";
            else if (pwd.innerHTML == "Strong")
                pwd.style.color = "Red";
        }

        function ButtonClicked(field) {
            document.getElementById('<%= lblMsg.ClientID %>').text = "";
            var newPwd = document.getElementById('<%= txtNewPassword.ClientID %>').value;
            var compPwd = document.getElementById('<%= txtConfirmPassword.ClientID %>').value;
            if (newPwd != compPwd) {
                alert("Password Missmatch");
                return false;
            }

            var strength = document.getElementById("PasswordStrength").innerHTML;


            if (strength.indexOf("Very Strong") < 0) {
                alert("Password is not strong enough!");
                return false;
            }
        }
        function CheckPassword(password) {
            var strength = new Array();
            strength[0] = "Blank";
            strength[1] = "Very Weak";
            strength[2] = "Weak";
            strength[3] = "Medium";
            strength[4] = "Strong";
            strength[5] = "Very Strong";

            var score = 1;

            if (password.length < 1)
                return strength[0];

            if (password.length < 4)
                return strength[1];

            if (password.length >= 5)
                score++;
            if (password.length >= 6)
                score++;
            if (password.match(/\d+/))
                score++;
            ////            if (password.match(/[a-z]/) &&
            ////		password.match(/[A-Z]/))
            ////                score++;
            if (password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]/))
                score++;

            return strength[score];
        }
    </script>
</head>
<body class="inner_wrappermain">
    <form id="Form1" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="email_login">
                    <%--<div class="logosub">
                        <img src="images/logo.png" width="100%" height="auto" /></div>--%>
                    <div class="email_temporary">
                        <div class="reset_pass">
                            <h3>
                                Please Reset Your Password</h3>
                            <asp:Label ID="lblPwdMsg" runat="server" Text="* The password must be 8 characters."
                                ForeColor="#FF3300"></asp:Label><br />
                            <asp:Label ID="lblpwd1Msg" runat="server" ForeColor="#FF3300" Text="* Password should contain atleast one character,"></asp:Label>
                            </br>
                            <asp:Label ID="lblpwd2Msg" runat="server" ForeColor="#FF3300" Text="  special symbol and one number...."></asp:Label>
                            <%--<h5>
                            These Questions help us to know your Identity</h5>--%>
                            <%--<p class="whatwas">What was your Childhood Nickname? </p>--%>
                            <%--<input class="user_box" type="text" value="Answer" name="user id">--%>
                            <br />
                            </br>
                            <asp:Label ID="lblNewPassword" CssClass="whatwas" Text="New Password" runat="server"></asp:Label>
                            <asp:TextBox class="user_box" runat="server" ID="txtNewPassword" data-bvalidator="required"
 TextMode="Password"
                                MaxLength="8"></asp:TextBox>
                            <span style="color: Red;" id="PasswordStrength"></span>
                            <%--<asp:RequiredFieldValidator ID="rfv_NewPassword" ControlToValidate="txtNewPassword"
                                runat="server" ErrorMessage="Enter password" ValidationGroup="a"></asp:RequiredFieldValidator>--%></p>
                            <p class="whatis">
                                <asp:Label ID="lblConfirmPassword" CssClass="whatwas" Text="Confirm Password" runat="server"></asp:Label>
                                <asp:TextBox class="user_box" runat="server" data-bvalidator="equalto[txtNewPassword],required"
 ID="txtConfirmPassword" TextMode="Password"></asp:TextBox>
                                <p class="whatis">
                                    <%--<asp:RequiredFieldValidator ID="rfvConfirmpwd" ControlToValidate="txtConfirmPassword"
                                        runat="server" ErrorMessage="Confirm password" ValidationGroup="a"></asp:RequiredFieldValidator>--%></p>
                                <p class="whatis">
                                    <%--<asp:CompareValidator ID="cv_CompareValidator" ControlToCompare="txtNewPassword"
                                    ControlToValidate="txtConfirmPassword" runat="server" ErrorMessage="Password Missmatch"
                                    ValidationGroup="a"></asp:CompareValidator>--%></p>
                                <%-- <input class="user_box" type="text" value="Answer" name="user id">--%>
                                <%--<asp:TextBox class="user_box" runat="server" value="Answer" ID="user"></asp:TextBox>--%>
                                <p class="forgot">
                                    <asp:Label ID="lblMsg" CssClass="whatwas" runat="server"></asp:Label></p>
                                <div class="clearfix">
                                </div>
                                <div class="back_btn">
                                    <a href="/_layouts/CustomLoginPage/EditUserProfileForPopUp.aspx">Back</a></div>
                                <asp:Button ID="btnSubmit" OnClientClick="return ButtonClicked()" OnClick="btnSubmit_Click"
                                    ValidationGroup="a" CssClass="continue_btn" runat="server" Text="Submit" />
                        </div>
                    </div>
                </div>
                <div id="part_off">
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
