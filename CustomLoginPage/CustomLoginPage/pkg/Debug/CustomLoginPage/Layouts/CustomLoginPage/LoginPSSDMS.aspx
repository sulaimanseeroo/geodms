<%@ Assembly Name="CustomLoginPage, Version=1.0.0.0, Culture=neutral, PublicKeyToken=7247f84a56c4f5f6" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPSSDMS.aspx.cs" Inherits="CustomLoginPage.Layouts.CustomLoginPage.LoginPSSDMS"  %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>PSS - DMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="faveicon.png" type="image/x-icon" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/view_port.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700'
        rel='stylesheet' type='text/css'>
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<form id="Form1" runat="server">
    <section id="section_log01">
<div class="logo"><img src="images/logo.png" width="64" height="96" alt=""/></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12"><div class="banner_log">
      <div id="circle">
  <div class="circle one"> <img src="images/sh_01.png" width="78" height="169"/></div>
    <div class="circle two"> <img src="images/sh_02.png" width="78" height="169"/></div>
  <div class="circle three"> <img src="images/sh_03.png" width="78" height="169"/></div>
  <div class="circle four"> <img src="images/sh_04.png" width="78" height="169"/></div>
</div>
     <div class="banner_pic"> <img src="images/pc_row.png" width="100%" height="auto" alt=""/></div></div>
          <div class="banner_mob"> <img src="images/pc_row_copy.png" width="100%" height="auto" alt=""/></div></div>

      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</section>
    <section id="section_log02">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-lg-offset-3">
        <div class="login_box">
        <div class="region"><img src="images/ranger.png"/></div>
                <div class="map_part"><img src="images/map_parj.png"/></div>

          <h1>login<span></span></h1>
          <div class="field_section_lft">
            <p>
            <asp:TextBox ID="txtUsername" CssClass="login_username" value="Username" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rquser" runat="server" ValidationGroup="a"
                    ControlToValidate="txtUsername" ErrorMessage="*"></asp:RequiredFieldValidator>
              <%--<input class="login_username" type="text" value="" name="_username" placeholder="User Name">--%>
            </p>
          </div>
          <div class="field_section_rit">
            <p>
            <asp:TextBox ID="txtPassWord" CssClass="login_password" TextMode="Password" runat="server"></asp:TextBox>
              <%--<input class="login_password" type="password" value="********" name="password">--%>
              <asp:RequiredFieldValidator ID="rqpassword" ValidationGroup="a" runat="server" 
                    ControlToValidate="txtPassWord" ErrorMessage="*"></asp:RequiredFieldValidator>
              <%--<input class="login_password" type="password" value="" name="password" placeholder="********">--%>
            </p>
          </div>
          <div class="signin_sec">
            <p>
            <asp:Button ID="btnLogin" ValidationGroup="a" runat="server" CssClass="sign_inbtn"  OnClick="btnLogin_Click" Text="SIGN IN"></asp:Button>
              <%--<a href="index.html" class="sign_inbtn">LOGIN</a>--%> <asp:Label ID="lblMsg" ForeColor="Red" runat="server" ></asp:Label>

            </p>
            <p class="chec_part">
             <asp:CheckBox ID="chkRemember" CssClass="remember_me" runat="server"></asp:CheckBox>
              <%--<input class="remember_me" type="checkbox" value="Remember Me" name="remember me">--%>
              Remember Me</p>
          </div>
          <div class="login_bottom">
            <p><span class="for_lft"><a href="http://sp01:24011/_layouts/CustomLoginPage/ForgotUsername.aspx" target="_blank">Forgot Username ?</a> </span> <span class="for_rit"> <asp:LinkButton ID="lnkForgotPwd" OnClick="lnkForgotPwd_Click" CssClass="forgotpass" runat="server">Forgot Password ?</asp:LinkButton>
            <%--<a href="forgot_password.html"> Forgot Password ? </a>--%></span> </p>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</section>
    <footer id="footer_sec">
      <div class="container">
    <div class="row">
          <div class="col-lg-12">
        <div class="copy_right">
              <p>© 2013  PSS - DMS. All Rights Reserved. </p>
            </div>
        <div class="social_mediasec">
              <div class="sm_icn"><a href="#"><img src="images/facebook.jpg" alt=""/></a></div>
              <div class="sm_icn"><a href="#"><img src="images/twitter.jpg" alt=""/></a></div>
              <div class="sm_icn"><a href="#"><img src="images/linkedin.jpg" alt=""/></a></div>
              <div class="sm_icn"><a href="#"><img src="images/skype.jpg" alt=""/></a></div>
            </div>
        <ul class="footer_menu">
              <li  class="first_child"><a href="#">Help</a></li>
              <li><a href="#">E - mail Admin</a></li>
            </ul>
      </div>
        </div>
  </div>
    </footer>
    <!--------------- toogle menu script --------------->
    <script type="text/javascript" src="js/script.js"></script>
    </form>
</body>
</html>
