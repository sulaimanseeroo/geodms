<%@ Assembly Name="CustomLoginPage, Version=1.0.0.0, Culture=neutral, PublicKeyToken=7247f84a56c4f5f6" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HelpForLogin.aspx.cs" Inherits="CustomLoginPage.Layouts.CustomLoginPage.HelpForLogin" %>

<html>
<head>
    <title>Help</title>
    <link rel="icon" href="/_layouts/CustomLoginPage/faveicon.png" type="image/x-icon" />
    <link href="/_layouts/CustomLoginPage/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/view_port.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700'
        rel='stylesheet' type='text/css' />
    <style>
        .email_login
        {
            top: 210px !important;
        }
        button, html input[type="button"], input[type="reset"], input[type="submit"]
        {
            background: none repeat scroll 0 0 #1F6FF7;
            border-bottom: 3px solid #0051CB;
            border-radius: 3px;
            color: #FFFFFF;
            display: block;
            float: right;
            font-size: 18px;
            font-weight: 500;
            line-height: 32px;
            margin-bottom: 30px;
            margin-left: 15px;
            margin-top: 10px;
            padding: 4px 10px;
            border-top: none;
            border-left: none;
            border-right: none;
            margin-right: 15px;
            text-align: center;
        }
    </style>
    <script type="text/javascript">
        function CloseModel() {
            $.modal.close();
            return false;
        }

    </script>
    <script src="/_layouts/CustomLoginPage/js/Accordian/modernizr.custom.js"></script>
    <script src="/_layouts/CustomLoginPage/js/Accordian/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/_layouts/CustomLoginPage/js/Accordian/bootstrap.js" type="text/javascript"></script>
    <script src="/_layouts/CustomLoginPage/js/Accordian/bootstrap.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {

            $("#toggleText").hide();
            $("#toggleText1").hide();
            $("#toggleText2").hide();
            $("#toggleText3").hide();
            $("#displayText").html("<img src='/_layouts/CustomLoginPage/images/Accordian/accordion_add.jpg' class='acc_plus' alt=''/>");
            $("#displayText1").html("<img src='/_layouts/CustomLoginPage/images/Accordian/accordion_add.jpg' class='acc_plus' alt=''/>");
            $("#displayText2").html("<img src='/_layouts/CustomLoginPage/images/Accordian/accordion_add.jpg' class='acc_plus' alt=''/>");
            $("#displayText3").html("<img src='/_layouts/CustomLoginPage/images/Accordian/accordion_add.jpg' class='acc_plus' alt=''/>");

            $("#displayText").click(function () {
                if (this.className.indexOf('clicked') != -1) {
                    $("#toggleText").toggle();
                    $(this).removeClass('clicked');
                    $(this).html("<img src='/_layouts/CustomLoginPage/images/Accordian/accordion_add.jpg' class='acc_plus' alt=''/>");
                }
                else {
                    $(this).addClass('clicked');
                    $("#toggleText").toggle();
                    $(this).html("<img src='/_layouts/CustomLoginPage/images/Accordian/accordion_minus.jpg' class='acc_plus' alt=''/>");
                }
            });
            $("#displayText1").click(function () {
                if (this.className.indexOf('clicked') != -1) {
                    $("#toggleText1").toggle();
                    $(this).removeClass('clicked');
                    $(this).html("<img src='/_layouts/CustomLoginPage/images/Accordian/accordion_add.jpg' class='acc_plus' alt=''/>");
                }
                else {
                    $(this).addClass('clicked');
                    $("#toggleText1").toggle();
                    $(this).html("<img src='/_layouts/CustomLoginPage/images/Accordian/accordion_minus.jpg' class='acc_plus' alt=''/>");
                }
            });
            $("#displayText2").click(function () {
                if (this.className.indexOf('clicked') != -1) {
                    $("#toggleText2").toggle();
                    $(this).removeClass('clicked');
                    $(this).html("<img src='/_layouts/CustomLoginPage/images/Accordian/accordion_add.jpg' class='acc_plus' alt=''/>");
                }
                else {
                    $(this).addClass('clicked');
                    $("#toggleText2").toggle();
                    $(this).html("<img src='/_layouts/CustomLoginPage/images/Accordian/accordion_minus.jpg' class='acc_plus' alt=''/>");
                }
            });
            $("#displayText3").click(function () {
                if (this.className.indexOf('clicked') != -1) {
                    $("#toggleText3").toggle();
                    $(this).removeClass('clicked');
                    $(this).html("<img src='/_layouts/CustomLoginPage/images/Accordian/accordion_add.jpg' class='acc_plus' alt=''/>");
                }
                else {
                    $(this).addClass('clicked');
                    $("#toggleText3").toggle();
                    $(this).html("<img src='/_layouts/CustomLoginPage/images/Accordian/accordion_minus.jpg' class='acc_plus' alt=''/>");
                }
            });
        });
    </script>
</head>
<body class="help_main">
    <%--<form id="Form1" runat="server">--%>
    <div id="wrap">
        <div id="accordion">
            <h3>
              Login<a href="javascript:void(0);" id="displayText"><img src="/_layouts/CustomLoginPage/images/Accordian/accordion_minus.jpg"
                     class="acc_plus" alt="" /></a></h3>
            <div id="toggleText">
                <p>
                    This is the Login page for PSS-DMS. Users can login with existing PSS-DMS credentials.
                                    Login details will have been provided to you by  Your administration team.
                                    </p>
                                    <img src="/_layouts/CustomLoginPage/images/login.jpg" alt="login" />
            </div>
            <h3>
                Credentials Recovery<a href="javascript:void(0);" id="displayText1"><img src="/_layouts/CustomLoginPage/images/Accordian/accordion_minus.jpg"
                    class="acc_plus" alt="" /></a></h3>
            <div id="toggleText1">
                <p>
                    In the Login page, there are links for Forgot Password and Forgot Username. If the
                                    password is Forgotten, then click the Forgot Password and if the username is forgotten,
                                    then click the Forgot Username.
                </p>
                <img src="/_layouts/CustomLoginPage/images/credentialrecovery.jpg" alt="credentialrecovery" />
            </div>
            <h3>
                Forgot Password<a href="javascript:void(0);" id="displayText2"><img src="/_layouts/CustomLoginPage/images/Accordian/accordion_minus.jpg"
                    class="acc_plus" alt="" /></a></h3>
            <div id="toggleText2">
                <p>
                   If the user select forgot password link, it will navigate to the password recovery
                                    page. In this page, user has to put his USERID to recover his password. There are
                                    3 ways.
                                    </br> • Ask Security Question which is supplied on the time of user registration.
                                    </br>
                                    • Email a temporary password to the user's email which is also supplied on the time
                                    of user registration. 
                                    </br>• SMS a temporary password to the user's Mobile Number which
                                    is supplied on the time of user registration.
                                </p>
            </div>
            <h3>
                Forgot Username<a href="javascript:void(0);" id="displayText3"><img  class="acc_plus" src="/_layouts/CustomLoginPage/images/Accordian/accordion_minus.jpg"
                    alt="" /></a></h3>
            <div id="toggleText3">
                <p>
                    
                                     In the login page, there is an option for Forgot Username. 
                                     If it is selected , it will navigates to the following page.
                                     Here the user is requested to submit his Email ID. 
                                     If the Email ID exists in the Database, then the corresponding username will be sent to the given Email ID.
                </p>
                <img src="/_layouts/CustomLoginPage/images/forgotusername.jpg" alt="forgotusername" />
            </div>
        </div>
    </div>
   <%-- </form>--%>
</body>
</html>
