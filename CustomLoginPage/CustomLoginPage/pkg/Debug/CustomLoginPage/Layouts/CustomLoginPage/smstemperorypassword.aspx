<%@ Assembly Name="CustomLoginPage, Version=1.0.0.0, Culture=neutral, PublicKeyToken=7247f84a56c4f5f6" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="smstemperorypassword.aspx.cs"
    Inherits="CustomLoginPage.Layouts.CustomLoginPage.smstemperorypassword" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>SMS Temporary Password | PSS - DMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="faveicon.png" type="image/x-icon" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/view_port.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700'
        rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="inner_wrappermain">
    <form runat="server">
    <div id="wrap">
    <div class="logo"><img src="images/logo.png" width="64" height="96" alt=""/></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="email_login">
                    <div class="email_body">
                    <div class="email_temporary">
                        <h3>
                            SMS Temporary Password</h3>
                        <p>
                            PSS - DMS will send a numeric code to your phone whenever you sign in from an untrusted
                            computer device.
                        </p>
                        <p>
                            <div class="email_text">
                                SMS :</div>
                            <div class="email_check">
                                <a href="#" class="question">
                                    <img src="images/qu1.jpg" onmouseover="this.src='images/qu01.jpg'" onmouseout="this.src='images/qu1.jpg'" /></a>
                                <a href="#" class="question">
                                    <img src="images/tic.jpg" onmouseover="this.src='images/tic01.jpg'" onmouseout="this.src='images/tic.jpg'" /></a>
                            </div>
                        </p>
                        <div class="clearfix">
                        </div>
                        <p>
                            <div class="phonenumber">
                                Phone Number</div>
                            <div class="example">
                                eg : (201) 555-0123</div>
                            <%--<input class="flag_field" type="text" value="(201) 456-035698" name="user id">--%>
                            <asp:TextBox runat="server" ID="txtphone" class="flag_field" value="(201) 456-035698"></asp:TextBox>
                        </p>
                        <div class="clearfix">
                        </div>
                        <div class="back_btn"><a href="/_layouts/CustomLoginPage/forgotpassword.aspx">Back</a></div>
                        <a href="#" class="continue_btn">SUBMIT</a>
                    </div>
                    </div>
                </div>
                <div id="part_off">
                </div>
            </div>
        </div>
    </div>
     </div>
    </form>
</body>
</html>
