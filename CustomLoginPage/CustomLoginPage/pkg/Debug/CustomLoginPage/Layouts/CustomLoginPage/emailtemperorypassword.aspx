<%@ Assembly Name="CustomLoginPage, Version=1.0.0.0, Culture=neutral, PublicKeyToken=7247f84a56c4f5f6" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="emailtemperorypassword.aspx.cs" Inherits="CustomLoginPage.Layouts.CustomLoginPage.emailtemperorypassword"%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Email Temporary Password | PSS - DMS</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="faveicon.png" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/view_port.css" rel="stylesheet" type="text/css">

<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700' rel='stylesheet' type='text/css'>
<link href="/_layouts/CustomLoginPage/css/bvalidator/bvalidator.css" rel="stylesheet" type="text/css" />
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery-latest.min.js" type="text/javascript"></script>
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery.bvalidator.js" type="text/javascript"></script>
 <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    

</head>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form1').bValidator();
    });
</script> 
<body class="inner_wrappermain">
<form id="form1" runat="server">
<div class="logo"><img src="images/logo.png" width="64" height="96" alt=""/></div>
<div class="container">
<div class="row">
<div class="col-lg-12">

<div class="email_login">
<div class="email_body">
<%--<div class="logosub"><img src="images/logo.png" width="100%" height="auto"/></div>--%>
 <div class="email_temporary">
 <h3>Email Temporary Password</h3>
 <h5> Password Reset Requested</h5>
 <p>Please enter the e-mail address used to login to the my PSS -DMS Portal for referring providers. A new password will be sent to that e-mail address. </p>
 <%--<p>
 <div class="email_text"> E-mail :</div> 
 <div class="email_check">
 <a href="#" class="question"><img src="images/qu1.jpg" 
onmouseover="this.src='images/qu01.jpg'"
onmouseout="this.src='images/qu1.jpg'" /></a>
 <a href="#" class="question"><img src="images/tic.jpg" 
onmouseover="this.src='images/tic01.jpg'"
onmouseout="this.src='images/tic.jpg'" /></a>
 </div> 
 </p>--%> 
  <div class="clearfix"></div>
 
 <p><%--<input class="user_box" type="text" value="Demo@gmail.com" name="user id">--%>
 <asp:TextBox runat="server" ID="txtemail" data-bvalidator="email,required" class="user_box" ></asp:TextBox>
 <%--<asp:RequiredFieldValidator ID="rfv_UserID" runat="server" ValidationGroup="a" ErrorMessage="Enter email and continue" ControlToValidate="txtemail"></asp:RequiredFieldValidator>--%>
  </p>
  <p>
 <%--<asp:RegularExpressionValidator ID="revemail" 
                runat="server" ErrorMessage="Please Enter Valid Email ID"
                    ValidationGroup="a" ControlToValidate="txtEmail" 
                    ForeColor="Red" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                    </asp:RegularExpressionValidator>--%>
 <div class="back_btn"><a href="/_layouts/CustomLoginPage/forgotpassword.aspx">Back</a></div>
 </p>
     <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" ValidationGroup="a" CssClass="continue_btn" runat="server" Text="Submit" />
<asp:Label ID="lblMsg" runat="server"
    Text="" ForeColor="Red"></asp:Label>
 </div>

  </div>
</div>


<div id="part_off"></div>

</div>
</div>
</div>
</form>
</body>
</html>

