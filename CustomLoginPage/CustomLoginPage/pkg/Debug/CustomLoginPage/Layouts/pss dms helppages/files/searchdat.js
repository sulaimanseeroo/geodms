var SearchFiles = ["credentials recovery.htm",
"email temporary password.htm",
"forgot password.htm",
"forgot username.htm",
"login.htm",
"security question.htm",
"sms temporary password.htm"];

var SearchTitles = ["Credentials Recovery",
"Email Temporary Password",
"Forgot Password",
"Forgot Username",
"Login",
"Security Question",
"SMS Temporary Password"];

var SearchIndexes = [["ADD",[4]],["ADMINISTRATION",[4]],["ADMINISTRATOR",[4]],["ALSO",[2]],["AND",[0,1,5,6]],["ANSWER",[5]],["ARE",[0,2]],["ASK",[2]],["AUTHORISED",[4]],["BUTTON",[5]],["CAN",[1,2,4,5,6]],["CHANGE",[1,6]],["CHANGED",[5]],["CHM",[0,1,2,3,4,5,6]],["CHM2WEB",[0,1,2,3,4,5,6]],["CHOOSE",[5]],["CLICK",[0]],["CLICKS",[5]],["CONFIRMS",[5]],["CONTAINS",[1,6]],["CONVERTED",[0,1,2,3,4,5,6]],["CORRECT",[5]],["CREDENTIALS",[0,4]],["DETAILS",[4]],["DOCUMENT",[4]],["EDIT",[4]],["ELSE",[5]],["EMAIL",[1,2]],["ENTER",[2]],["ERROR",[5]],["EXISTING",[4]],["EXISTS",[5]],["FIGURE",[2,3,5]],["FOLLOWING",[3]],["FOR",[0,2,3]],["FORGOT",[0,2,3]],["FORGOTTEN",[0]],["FROM",[0,1,2,3,4,5,6]],["GENERATE",[5]],["GIVEN",[5]],["HAS",[2]],["HERE",[5]],["HIS",[1,5,6]],["HTML",[0,1,2,3,4,5,6]],["LINK",[0,1,2,5,6]],["LINKS",[0]],["LOCATE",[4]],["LOG",[1,6]],["LOGIN",[0,3,4]],["MANAGEMENT",[4]],["MESSAGE",[5]],["MOBILE",[2]],["NAVIGATE",[2]],["NAVIGATES",[3,5]],["NEW",[5]],["NOTE",[4]],["NUMBER",[2]],["ONE",[2]],["ONLY",[2,4]],["OPTION",[2,3,5]],["OPTIONS",[2]],["PAGE",[0,2,3,4,5]],["PASSWORD",[0,1,2,5,6]],["PERSON",[4]],["PRO",[0,1,2,3,4,5,6]],["PROVIDED",[4]],["QUESTION",[2,5]],["RECEIVING",[1,6]],["RECOVER",[2]],["RECOVERY",[0,2]],["REGISTRATION",[2]],["REQUESTED",[5]],["RESET",[5]],["RETRIEVE",[5]],["SEARCH",[4]],["SECURITY",[2,5]],["SELECT",[2]],["SELECTED",[2,3]],["SELECTS",[1,5,6]],["SHOULD",[4]],["SHOWN",[2,5]],["SITE",[1,6]],["SMS",[2,6]],["SUBMIT",[5]],["SUPPLIED",[2]],["SYSTEM",[4]],["TEAM",[4]],["TEMPERARY",[1,6]],["TEMPORARY",[1,2,6]],["THE",[0,1,2,3,4,5,6]],["THEN",[0,4,5]],["THERE",[0,2,3]],["THEY",[2]],["THIS",[1,2,4,5,6]],["TIME",[2]],["UNICODE",[0,1,2,3,4,5,6]],["USER",[1,2,4,5,6]],["USERID",[2,5]],["USERNAME",[0,3]],["USERS",[4]],["USING",[1,6]],["WANTS",[4]],["WAYS",[2]],["WHEN",[5]],["WHICH",[1,2,6]],["WILL",[1,2,3,5,6]],["WITH",[0,1,2,3,4,5,6]],["WOULD",[4]],["YOU",[4]],["YOUR",[4]]];