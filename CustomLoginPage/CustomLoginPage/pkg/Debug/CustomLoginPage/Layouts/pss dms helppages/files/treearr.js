var TITEMS = [ 
 ["Login", null, "1",
  ["How to Login", "login.htm", "11"]
 ],
 ["Credential Recovery", null, "1",
  ["Credentials", "credentials recovery.htm", "11"],
  ["Password recovery", "forgot password.htm", "1",
   ["Forgot Password", "forgot password.htm", "11"],
   ["Password Recovery Methods", null, "1",
    ["Security Question", "security question.htm", "11"],
    ["Email Temporary Password", "email temporary password.htm", "11"],
    ["SMS Temporary Password", "sms temporary password.htm", "11"]
   ]
  ],
  ["Forgot username", "forgot username.htm", "11"]
 ]
];


var FITEMS = arr_flatten(TITEMS);

function arr_flatten (x) {
   var y = []; if (x == null) return y;
   for (var i=0; i<x.length; i++) {
      if (typeof(x[i]) == "object") {
         var flat = arr_flatten(x[i]);
         for (var j=0; j<flat.length; j++)
             y[y.length]=flat[j];
      } else {
         if ((i%3==0))
          y[y.length]=x[i+1];
      }
   }
   return y;
}

