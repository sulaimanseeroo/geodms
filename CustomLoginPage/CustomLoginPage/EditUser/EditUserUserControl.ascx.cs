﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Configuration;

namespace CustomLoginPage.EditUser
{
    public partial class EditUserUserControl : UserControl
    {
        ConnectionClass obj=new ConnectionClass();
        ConnectionClass obj1 = new ConnectionClass();
        public static string userName;
        public static DateTime createdDate;
        public static int Flag;
        bool Isexist = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                userName = Session["username"].ToString();
                createdDate = DateTime.Now;
                BindEditDetails();
                //BindSecurityQuestion();
                //if (CheckLogin())
                //{
                //    SecurityDiv.Visible = true;
                //}
                //else
                //{
                //    SecurityDiv.Visible = false;
                //}
            }
        }

        //private bool CheckLogin()
        //{
        //    Flag = 0;
        //    obj1.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = "MyAppName";
        //    obj1.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
        //    obj1.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
        //    obj1.cmd.Parameters.Add("@UpdateLastActivity", SqlDbType.Bit).Value = 0;
        //    DataTable dtSecurityQstn = obj.ViewData("aspnet_Membership_GetUserByName");
        //    if (dtSecurityQstn.Rows.Count > 0)
        //    {
        //        if (dtSecurityQstn.Rows[0]["LastLoginDate"] == null)
        //        {
        //            Flag = 2;
        //            Isexist = true;
        //        }
        //        else
        //        {
        //            Flag = 1;
        //            Isexist = false;
        //        }
        //    }
        //    return Isexist;
        //}

        private void BindEditDetails()
        {
            obj.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
            obj.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
            obj.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
            obj.cmd.Parameters.Add("@UpdateLastActivity", SqlDbType.Bit).Value = 0;
            DataTable EditUser = obj.ViewData("aspnet_Membership_GetUserByName");
            txtFirstName.Text = EditUser.Rows[0]["FirstName"].ToString();
            txtLastName.Text = EditUser.Rows[0]["LastName"].ToString();
            txtMobilePhone.Text = EditUser.Rows[0]["MobilePhone"].ToString();
            txtWorkPhone.Text = EditUser.Rows[0]["WorkPhone"].ToString();
            txtEmail.Text = EditUser.Rows[0]["Email"].ToString();
        }

        //private void BindSecurityQuestion()
        //{
        //    DataTable dtSecurityQuestions = obj1.ViewData("sp_GetSecurityQuestions");
        //    if (dtSecurityQuestions.Rows.Count > 0)
        //    {
        //        lblSecurityQuestion1.Text = dtSecurityQuestions.Rows[0]["SecurityQuestion"].ToString();
        //        lblSecurityQuestion2.Text = dtSecurityQuestions.Rows[1]["SecurityQuestion"].ToString();
        //    }
        //} 

        protected void btnEditUser_Click(object sender, EventArgs e)
        {

            obj.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
                obj.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;                
                obj.cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = txtEmail.Text;
                obj.cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = txtFirstName.Text;
                obj.cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = txtLastName.Text;
                obj.cmd.Parameters.Add("@MobilePhone", SqlDbType.VarChar).Value = txtMobilePhone.Text;
                obj.cmd.Parameters.Add("@WorkPhone", SqlDbType.VarChar).Value = txtWorkPhone.Text;
                obj.cmd.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = createdDate;
                obj.cmd.Parameters.Add("@LastActivityDate", SqlDbType.DateTime).Value = createdDate;
                obj.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
                //obj.cmd.Parameters.Add("@SecurityQuestion1", SqlDbType.VarChar).Value = lblSecurityQuestion1.Text;
                //obj.cmd.Parameters.Add("@SecurityQuestion2", SqlDbType.VarChar).Value = lblSecurityQuestion2.Text;
                //obj.cmd.Parameters.Add("@SecurityAnswer1", SqlDbType.VarChar).Value = txtSecurityQuestion1.Text;
                //obj.cmd.Parameters.Add("@SecurityAnswer2", SqlDbType.VarChar).Value = txtSecurityQuestion2.Text;
                obj.cmd.Parameters.Add("@flag", SqlDbType.Int).Value = Flag;
                if (obj.exequery("sp_UpdateUser"))
                {
                    lblMsg.Text="Your profile is successfully updated";
                }
        }
    }
}
