﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditUserUserControl.ascx.cs"
    Inherits="CustomLoginPage.EditUser.EditUserUserControl" %>
<head>
    <link rel="icon" href="/_layouts/CustomLoginPage/faveicon.png" type="image/x-icon" />
    <link href="/_layouts/CustomLoginPage/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/view_port.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700'
        rel='stylesheet' type='text/css' />
        <style>
        .email_login 
        {
            top:210px!important;
        }
        </style>
</head>
<body class="inner_wrappermain">
    <div id="wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="email_login">
                        <div class="email_temporary">
                            <h1>
                                Edit User</h1>
                            <h5>
                            </h5>
                            <p>
                                <span class="user_inn">First Name</span>
                                <asp:TextBox ID="txtFirstName" CssClass="user_innbox" runat="server"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Last Name</span>
                                <asp:TextBox ID="txtLastName" CssClass="user_innbox" runat="server"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Email<asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                    runat="server" ControlToValidate="txtEmail" ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator></span>
                                <asp:TextBox ID="txtEmail" CssClass="user_innbox" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="a"
                                    ControlToValidate="txtEmail" ErrorMessage="Invalid Email ID" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Mobile Phone</span>
                                <asp:TextBox ID="txtMobilePhone" CssClass="user_innbox" runat="server"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Work Phone</span>
                                <asp:TextBox ID="txtWorkPhone" runat="server" CssClass="user_innbox"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <%--<div id="SecurityDiv" runat="server">
                                <p>
                                    <span class="user_inn">Security Question1</span>
                                    <asp:Label ID="lblSecurityQuestion1" runat="server"></asp:Label>
                                </p>
                                <p>
                                    <span class="user_inn">Answer<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                        runat="server" ControlToValidate="txtSecurityQuestion1" ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator></span>
                                    <asp:TextBox ID="txtSecurityQuestion1" CssClass="user_innbox" runat="server"></asp:TextBox>
                                </p>
                                <p>
                                    <span class="user_inn">Security Question2</span>
                                    <asp:Label ID="lblSecurityQuestion2" runat="server"></asp:Label>
                                </p>
                                <p>
                                    <span class="user_inn">Answer
                                        <asp:RequiredFieldValidator ID="rfv_txtSecurityQuestion2" runat="server" ControlToValidate="txtSecurityQuestion2"
                                            ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator></span>
                                    <asp:TextBox ID="txtSecurityQuestion2" CssClass="user_innbox" runat="server"></asp:TextBox>
                                </p>
                            </div>--%>
                            <%--<p>
                                <span class="user_inn">Answer<asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                    runat="server" ControlToValidate="txtSecurityAnswer" ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator></span>
                                <asp:TextBox ID="txtSecurityAnswer" CssClass="user_innbox" runat="server"></asp:TextBox></p>--%>
                            <div class="clearfix">
                            </div>
                            <%--<div class="back_btn">
                                <a href="forgot_password.html">Back</a></div>--%>
                            <asp:Button ID="btnEditUser" runat="server" ValidationGroup="a" Text="Update User"
                                CssClass="continue_btn" OnClick="btnEditUser_Click" />
                            <asp:Label ID="lblMsg" runat="server"></asp:Label>
                            <%--<a href="#" class="continue_btn">SUBMIT</a>--%>
                        </div>
                    </div>
                    <div id="part_off">
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
