<%@ Assembly Name="CustomLoginPage, Version=1.0.0.0, Culture=neutral, PublicKeyToken=7247f84a56c4f5f6" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddUser.aspx.cs" Inherits="CustomLoginPage.Layouts.CustomLoginPage.AddUser" %>

<html>
<head>
<title>Add New User | PSS - DMS</title>
    <link rel="icon" href="/_layouts/CustomLoginPage/faveicon.png" type="image/x-icon" />
    <link href="/_layouts/CustomLoginPage/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/view_port.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script src="http://code.jquery.com/jquery-1.7.1.js" type="text/javascript"></script>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700'
        rel='stylesheet' type='text/css' />
    <style>
        .email_login
        {
            top: 250px !important;
        }
    </style>
    <script type="text/javascript">
       function PasswordChanged(field) {
            var pwd = document.getElementById("PasswordStrength");
            pwd.innerHTML = CheckPassword(field.value);
            if (pwd.innerHTML == "Very Strong")
                pwd.style.color = "Green";
            else if (pwd.innerHTML == "Strong")
                pwd.style.color = "Red";
                        }

        function ButtonClicked(field) {
            var strength = document.getElementById("PasswordStrength").innerHTML;

            if (strength.indexOf("Strong") < 0) {
                alert("Password is not strong enough!");
                return false;
            }
        }
        function CheckPassword(password) {
            var strength = new Array();
            strength[0] = "Blank";
            strength[1] = "Very Weak";
            strength[2] = "Weak";
            strength[3] = "Medium";
            strength[4] = "Strong";
            strength[5] = "Very Strong";

            var score = 1;

            if (password.length < 1)
                return strength[0];

            if (password.length < 4)
                return strength[1];

            if (password.length >= 5)
                score++;
            if (password.length >= 6)
                score++;
            if (password.match(/\d+/))
                score++;
//            if (password.match(/[a-z]/) &&
//		password.match(/[A-Z]/))
//                score++;
            if (password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]/))
                score++;

            return strength[score];
        }
	</script>
    <script src="/_layouts/CustomLoginPage/js/jquery.simplemodal.1.4.1.min.js" type="text/javascript"></script>
    <%--<link rel="stylesheet" type="text/css" media="screen" href="/_layouts/CustomLoginPage/css/jquery.validate.password.css" />
    <script type="text/javascript" src="/_layouts/CustomLoginPage/js/lib/jquery.js"></script>
    <script type="text/javascript" src="/_layouts/CustomLoginPage/js/lib/jquery.validate.js"></script>
    <script type="text/javascript" src="/_layouts/CustomLoginPage/js/jquery.validate.password.js"></script>
    <script id="demo" type="text/javascript">
        $(document).ready(function () {
            $("#register").validate();
            $("#txtPassWord").valid();

        });
</script>
    <style>
        span, input
        {
            float: left;
        }
        input
        {
            margin-left: 1em;
        }
        span.error
        {
            display: none !important;
        }
        .password-meter
        {
            float: left;
        }
    </style>--%>
     <link href="/_layouts/CustomLoginPage/css/bvalidator/bvalidator.css" rel="stylesheet" type="text/css" />
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery-latest.min.js" type="text/javascript"></script>
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery.bvalidator.js" type="text/javascript"></script>
</head>
<script type="text/javascript">
    var options = {
        offset: { x: 5, y: -2 },
        position: { x: 'left', y: 'top' }
    };
    $(document).ready(function () {
        $('#form1').bValidator(options);
    });
</script> 
<body class="inner_wrappermain">
    <form runat="server" id="form1">
    <div id="wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="email_login" style="height:660px !important;">
                        <div class="email_temporary">
                            <h3>
                                New User</h3>
                            <h5>
                            </h5>
                            <p><asp:Label ID="lblMsg" runat="server" ForeColor="#009933" Font-Size="Medium" Font-Bold="True"></asp:Label></p>
                            <p>
                                <span class="user_inn">First Name</span>
                                <asp:TextBox ID="txtFirstName" CssClass="user_innbox" data-bvalidator="required" runat="server"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Last Name</span>
                                <asp:TextBox ID="txtLastName" CssClass="user_innbox" data-bvalidator="required" runat="server"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Username<%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                    runat="server" ControlToValidate="txtUserName" ValidationGroup="a" ErrorMessage="*"></asp:RequiredFieldValidator>--%></span>
                                <asp:TextBox ID="txtUserName" CssClass="user_innbox" data-bvalidator="required" runat="server"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Password</span>
                                <asp:TextBox ID="txtPassWord" runat="server" CssClass="user_innbox" data-bvalidator="required" MaxLength="8" TextMode="Password"></asp:TextBox>
                                <span style="color:Red;" id="PasswordStrength"></span>
                                <%--<div class="password-meter">
                                    <div class="password-meter-message">
                                        &nbsp;</div>
                                    <div class="password-meter-bg">
                                        <div class="password-meter-bar">
                                        </div>
                                    </div>
                                </div>--%>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Confirm Password</span>
                                <asp:TextBox ID="txtConfirmPwd" CssClass="user_innbox" runat="server" TextMode="Password" data-bvalidator="equalto[txtPassWord],required"></asp:TextBox>
                                <%--<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassWord"
                                    ControlToValidate="txtConfirmPwd" ErrorMessage="Password Missmatch"></asp:CompareValidator>--%>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <%-- <p>
                                <span class="user_inn">Security Question<asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                    runat="server" ControlToValidate="txtSecurityQstn" ErrorMessage="*"></asp:RequiredFieldValidator></span>
                                <asp:TextBox ID="txtSecurityQstn" CssClass="user_innbox" runat="server" TextMode="MultiLine"></asp:TextBox>
                                <input class="user_innbox" type="text"
                                    value="" name="user id"></p>
                            <p>
                                <span class="user_inn">Answer<asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                    runat="server" ControlToValidate="txtSecurityAnswer" ErrorMessage="*"></asp:RequiredFieldValidator></span>
                                <asp:TextBox ID="txtSecurityAnswer" CssClass="user_innbox" runat="server"></asp:TextBox>
                                <input
                                        class="user_innbox" type="text" value="" name="user id"></p>--%>
                            <p>
                                <span class="user_inn">IsApproved</span>
                                <asp:CheckBox ID="chkIsApproved" runat="server" />
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Email<%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                    runat="server" ControlToValidate="txtEmail" ErrorMessage="*"></asp:RequiredFieldValidator>--%></span>
                                <asp:TextBox ID="txtEmail" CssClass="user_innbox" data-bvalidator="email,required" runat="server"></asp:TextBox>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                                    ErrorMessage="Invalid Email ID" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="a"></asp:RegularExpressionValidator>--%>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Select Role</span>
                                <asp:DropDownList ID="ddlRole" CssClass="RoleDropdown" runat="server" Width="150px">
                                </asp:DropDownList>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Mobile Phone</span>
                                <asp:TextBox ID="txtMobilePhone" CssClass="user_innbox" data-bvalidator="required" runat="server"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Work Phone</span>
                                <asp:TextBox ID="txtWorkPhone" runat="server" CssClass="user_innbox"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Profile Picture</span>
                                <asp:FileUpload ID="fupProfilePicture" runat="server" />
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <div class="clearfix">
                            </div>
                            <%--<div class="back_btn">
                                <a href="forgot_password.html">Back</a></div>--%>
                            <asp:Button ID="btnAddUser" CssClass="continue_btn" runat="server" ValidationGroup="a" OnClick="btnAddUser_Click" OnClientClick="return ButtonClicked()"
                                Text="Add User" />
                            <div class="back_btn">
                                <%--<asp:HyperLink ID="hpkContinue"  NavigateUrl="http://sp01:24011/_layouts/CustomLoginPage/IndexPage.aspx"
                                    runat="server">Back</asp:HyperLink>--%>
                                <%-- <asp:LinkButton ID="lnkContinue" class="add" OnClientClick="return parent.CloseModel();" runat="server">Close</asp:LinkButton>--%>
                            </div>
                            
                            <%--<a href="#" class="continue_btn">SUBMIT</a>--%>
                        </div>
                    </div>
                    <div id="part_off">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
    <script type="text/javascript">
        _uacct = "UA-2623402-1";
        urchinTracker();
</script>
</body>
</html>
