<%@ Assembly Name="CustomLoginPage, Version=1.0.0.0, Culture=neutral, PublicKeyToken=7247f84a56c4f5f6" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotUsername.aspx.cs"
    Inherits="CustomLoginPage.Layouts.CustomLoginPage.ForgotUsername" %>

<html>
<head>
    <meta charset="utf-8">
    <title>Forgot Username | PSS - DMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="faveicon.png" type="image/x-icon" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/view_port.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700'
        rel='stylesheet' type='text/css'>
    <link href="/_layouts/CustomLoginPage/css/bvalidator/bvalidator.css" rel="stylesheet"
        type="text/css" />
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery-latest.min.js" type="text/javascript"></script>
    <script src="/_layouts/CustomLoginPage/js/bvalidator/jquery.bvalidator.js" type="text/javascript"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <style>
        #lblMsg
        {
            float: left;
            font-size: 16px;
        }
    </style>
</head>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form1').bValidator();
    });
</script>
<body class="inner_wrappermain">
    <form runat="server" id="form1">
    <div class="logo">
        <img src="images/logo.png" width="64" height="96" alt="" /></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="email_login">
                    <div class="email_body">
                        <div class="email_temporary">
                            <h3>
                                Forgot Username ?</h3>
                            <h5>
                                We will locate your account information and send you an e-mail with your username</h5>
                            <p class="whatis">
                                Please enter your e-mail<%--<input class="user_box" type="text" value="Enter your E - mail" name="user id">--%>
                                <asp:TextBox ID="txtEmail" CssClass="user_box" data-bvalidator="email,required" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="rfv_txtEmail" ValidationGroup="submit" runat="server"
                                    ControlToValidate="txtEmail" Display="Dynamic" Font-Size="16px" ErrorMessage="Enter email and continue"></asp:RequiredFieldValidator>--%>
                            </p>
                            <p>
                                <%--<asp:RegularExpressionValidator ID="revemail" runat="server" ErrorMessage="Please Enter Valid Email ID"
                                    ValidationGroup="submit" ControlToValidate="txtEmail" Font-Size="16px" Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                </asp:RegularExpressionValidator>--%>
                                <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="#006600"></asp:Label>
                            </p>
                            <div class="clearfix">
                            </div>
                            <div class="back_btn">
                                <a href="/_layouts/CustomLoginPage/LoginPage.aspx">Back</a></div>
                            <asp:Button ID="btnSubmit" CssClass="continue_btn" ValidationGroup="submit" runat="server"
                                Text="Submit" OnClick="btnSubmit_Click" />
                            <%--<a href="#" class="continue_btn">SUBMIT</a>--%>
                        </div>
                    </div>
                </div>
                <div id="part_off">
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
