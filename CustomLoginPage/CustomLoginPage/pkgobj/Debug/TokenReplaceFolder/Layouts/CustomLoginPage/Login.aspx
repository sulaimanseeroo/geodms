<%@ Assembly Name="CustomLoginPage, Version=1.0.0.0, Culture=neutral, PublicKeyToken=7247f84a56c4f5f6" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Import Namespace="Microsoft.SharePoint.WebControls" %>
<%@ Assembly Name="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Assembly Name="Microsoft.SharePoint.IdentityModel, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CustomLoginPage.Layouts.CustomLoginPage.Login" %>

<html>
<head id="Head1" runat="server">
    <title>Login Page </title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .textBoxStyle
        {
            border-width: 1px;
            border-style: solid;
            border-color: #A3A0A1;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="border-width: 1px; border-style: solid; border-color: #A3A0A1; padding: 20px;">
        <table class="style1">
            <tr>
                <td>
                    UserID
                </td>
                <td>
                    <asp:TextBox ID="TextBox1" CssClass="textBoxStyle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Password
                </td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server" CssClass="textBoxStyle" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Submit" />
                </td>
            </tr>
        </table>
    </div>
    <asp:PasswordRecovery ID="Passwordrecovery1" runat="server">
    </asp:PasswordRecovery>
    <div id="ChangePassword" style="width: 300px; height: auto; margin-top: 15%;">
        <table border="0" cellpadding="1" cellspacing="0" style="margin: 20px; width: 100%">
            <tr>
                <td align="left">
                    Username
                </td>
                </tr>
                <tr>
                <td>
                    <asp:TextBox ID="txtUname" Width="300px" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUname"
                        ErrorMessage="Username is required." ToolTip="Password is required." ValidationGroup="ChangePassword">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <%--<tr>
                                    <td align="left">
                                        <asp:Label ID="CurrentPasswordLabel" runat="server" ForeColor="White">Password:</asp:Label>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                        <asp:TextBox ID="CurrentPassword" Width="300px" runat="server" TextMode="Password"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                                            ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ChangePassword">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>--%>
            <tr>
                <td align="left">
                    New Password:
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="NewPassword"
                        ControlToValidate="ConfirmNewPassword" ErrorMessage="The confirm new password must match the new password."
                        ToolTip="The confirm new password must match the new password." ValidationGroup="ChangePassword">*</asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="NewPassword" runat="server" Width="300px" TextMode="Password"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                        ErrorMessage="New password is required." ToolTip="New password is required."
                        ValidationGroup="ChangePassword">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="left">
                    Confirm New Password:
                    <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                        ControlToValidate="ConfirmNewPassword" ErrorMessage="The confirm new password must match the new password."
                        ToolTip="The confirm new password must match the new password." ValidationGroup="ChangePassword">*</asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="ConfirmNewPassword" Width="300px" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                        ErrorMessage="Confirm new password is required." ToolTip="Confirm new password is required."
                        ValidationGroup="ChangePassword">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="center" style="color: Red; font-size: small;">
                    <asp:Literal ID="FailureTextPasswordChange" runat="server" EnableViewState="False"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td align="right" style="padding-top: 10px">
                    <asp:Button ID="ChangePasswordPushButton" runat="server" OnClick="ChangePasswordBtn_Click"
                        Text="Change Password" ValidationGroup="ChangePassword" Padding-Right="5px" />
                    <%--<asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" OnClick="Cancel_Click"
                                         Text="Cancel" />--%>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
