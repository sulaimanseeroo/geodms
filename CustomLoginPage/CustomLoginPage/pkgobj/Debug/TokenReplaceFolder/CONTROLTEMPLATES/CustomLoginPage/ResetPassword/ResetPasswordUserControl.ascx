<%@ Assembly Name="CustomLoginPage, Version=1.0.0.0, Culture=neutral, PublicKeyToken=7247f84a56c4f5f6" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResetPasswordUserControl.ascx.cs"
    Inherits="CustomLoginPage.ResetPassword.ResetPasswordUserControl" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
</style>
<head>
    <link rel="icon" href="/_layouts/CustomLoginPage/faveicon.png" type="image/x-icon" />
    <link href="/_layouts/CustomLoginPage/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/view_port.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/_layouts/CustomLoginPage/css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700'
        rel='stylesheet' type='text/css' />
</head>
<body class="inner_wrappermain">
    <div id="wrap">
        <div class="logo" style="margin-top:30px;">
            <img src="/_layouts/CustomLoginPage/images/logo.png" width="64" height="96" alt="" /></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="email_login">
                        <div class="email_temporary">
                            <h2>
                                Reset Password</h2>
                            <h5>
                            </h5>
                            <p>
                                <span class="user_inn">Old Password</span><asp:TextBox ID="txtOldPassword" CssClass="user_innbox"
                                    runat="server" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfv_OldPassword" ControlToValidate="txtOldPassword"
                                    runat="server" ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator>
                                <%--<input class="user_innbox" type="text" value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">New Password</span>
                                <asp:TextBox ID="txtNewPassword" CssClass="user_innbox" runat="server" TextMode="Password"></asp:TextBox>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <p>
                                <span class="user_inn">Confirm Password</span>
                                <asp:TextBox ID="txtConfirmPassword" CssClass="user_innbox" runat="server"
                                TextMode="Password"></asp:TextBox>
                            <asp:CompareValidator ID="cv_CompareValidator" ControlToCompare="txtNewPassword"
                                ControlToValidate="txtConfirmPassword" runat="server" ErrorMessage="Password Missmatch"
                                ValidationGroup="a"></asp:CompareValidator>
                                <%--<input class="user_innbox" type="text"
                                    value="" name="user id">--%></p>
                            <%--<p>
                                <span class="user_inn">Demo Lorem ipsum</span><input class="user_innbox" type="text"
                                    value="" name="user id"></p>--%>
                            <div class="clearfix">
                            </div>
                            <%--<div class="back_btn">
                                <a href="forgot_password.html">Back</a></div>--%>
                            <%--<a href="#" class="continue_btn">SUBMIT</a>--%>
                            <asp:Button ID="btnReset" CssClass="continue_btn" runat="server" OnClick="btnReset_Click"
                                Text="Reset" ValidationGroup="a" />
                            <asp:Label ID="lblMsg" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div id="part_off">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--<footer id="footer_sec">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="copy_right">
          <p>© 2013  PSS - DMS. All Rights Reserved. </p>
        </div>
        <div class="social_mediasec">
          <div class="sm_icn"><a href="#"><img src="images/facebook.jpg" alt=""/></a></div>
          <div class="sm_icn"><a href="#"><img src="images/twitter.jpg" alt=""/></a></div>
          <div class="sm_icn"><a href="#"><img src="images/linkedin.jpg" alt=""/></a></div>
          <div class="sm_icn"><a href="#"><img src="images/skype.jpg" alt=""/></a></div>
        </div>
        <ul class="footer_menu">
          <li  class="first_child"><a href="#">Help</a></li>
          <li><a href="#">E - mail Admin</a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>--%>
</body>
