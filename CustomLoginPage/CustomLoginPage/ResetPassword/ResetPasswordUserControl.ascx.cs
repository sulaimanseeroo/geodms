﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Configuration;

namespace CustomLoginPage.ResetPassword
{
    public partial class ResetPasswordUserControl : UserControl
    {
        ConnectionClass obj = new ConnectionClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            DateTime createdDate = DateTime.Now;
            string userName = Session["username"].ToString();
            obj.cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ConfigurationSettings.AppSettings["appName"];
            obj.cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
            obj.cmd.Parameters.Add("@OldPassword", SqlDbType.VarChar).Value = txtOldPassword.Text;
            obj.cmd.Parameters.Add("@NewPassword", SqlDbType.VarChar).Value = txtConfirmPassword.Text;
            obj.cmd.Parameters.Add("@CurrentTimeUtc", SqlDbType.DateTime).Value = createdDate;
            if (obj.exequery("aspnet_Membership_ResetPassword"))
            {
                lblMsg.Text = "Your password is successfully reset";
            }
        }
    }
}
